// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes',
  'app.directives','app.services', 'ngResource', 'ngStorage', 'pdf-viewer',
  'ngCordova', 'onezone-datepicker', 'ngAnimate', 'ngSanitize', 'toastr',
  'plgn.ionic-segment', 'ionic-datepicker'])

.config(function($ionicConfigProvider, toastrConfig, ionicDatePickerProvider, $httpProvider){
    $ionicConfigProvider.backButton.text('').previousTitleText(false);
    $ionicConfigProvider.views.swipeBackEnabled(true);
    $ionicConfigProvider.views.forwardCache(false);
    $httpProvider.interceptors.push('httpRequestInterceptor');

    /*Default Settings for toast*/
    angular.extend(toastrConfig, {
      autoDismiss: false,
      containerId: 'toast-container',
      maxOpened: 0,
      newestOnTop: true,
      positionClass: 'toast-bottom-center',
      preventDuplicates: false,
      preventOpenDuplicates: true,
      target: 'body'
    });
    angular.extend(toastrConfig, {
      allowHtml: false,
      closeButton: true,
      closeHtml: '<button>&times;</button>',
      extendedTimeOut: 1000,
      iconClasses: {
        error: 'toast-error',
        info: 'toast-info',
        success: 'toast-success',
        warning: 'toast-warning'
      },
      messageClass: 'toast-message',
      onHidden: null,
      onShown: null,
      onTap: null,
      progressBar: false,
      tapToDismiss: true,
      templates: {
        toast: 'directives/toast/toast.html',
        progressbar: 'directives/progressbar/progressbar.html'
      },
      timeOut: 5000,
      titleClass: 'toast-title',
      toastClass: 'toast'
    });

    var datePickerObj = {
      inputDate: new Date(),
      titleLabel: 'Selecciona una fecha',
      setLabel: 'OK',
      todayLabel: 'Hoy',
      closeLabel: 'Cerrar',
      mondayFirst: false,
      weeksList: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
      monthsList: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      templateType: 'modal',
      from: new Date(2012, 8, 1),
      to: new Date(),
      showTodayButton: true,
      dateFormat: 'dd/MM/yyyy',
      closeOnSelect: false,
      disableWeekdays: []
    };
    ionicDatePickerProvider.configDatePicker(datePickerObj);
})

.run(function($ionicPlatform, toastr, $rootScope, $ionicPopup, $ionicHistory,
              $cordovaStatusbar, $cordovaSpinnerDialog, $localStorage, DatosFactory) {
  $rootScope.$on('$stateChangeStart', function () {
      toastr.clear();
    })

  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      //StatusBar.styleDefault();
      //$cordovaStatusbar.style(1);
      //StatusBar.hide();

    }
    if(navigator.splashscreen){
      setTimeout(function() {
        navigator.splashscreen.hide();
      }, 100);
    }

    $rootScope.$on('$cordovaInAppBrowser:loadstart', function(e, event){
        $cordovaSpinnerDialog.hide();
      });
    if($localStorage.id_avance == undefined){
      $localStorage.id_avance = 0;
    }

    //
    screen.orientation.lock('portrait');
  });

   $ionicPlatform.registerBackButtonAction(function(event){
      if ($ionicHistory.currentStateName() == 'menu.locales'){
        event.preventDefault();
      } else if($ionicHistory.currentStateName() == 'menu.fiscalizacion'){

      } else {
        $ionicHistory.goBack();
      }
    },100);

})
.filter('avanceFilter', function() {
  return function(items, botones) {
    var filtered = [];
    if(botones && (botones.a == true || botones.b == true || botones.c == true)){
        angular.forEach(items, function(el) {
            var factor = el.factor_intervencion;
            var cantidad = el.cantidad;
            var porcentaje = (factor/cantidad)*100;
            var entro = false;
            if(botones.a == true && (cantidad == undefined || cantidad == 0)){
              filtered.push(el);
              entro = true;
            }
            if(porcentaje >= 100 && botones.c == true && !entro){
                    filtered.push(el);
                    entro = true;
            }else if(( porcentaje >= 50 && porcentaje < 100 && botones.b == true && !entro)){
                    filtered.push(el);
                    entro = true;
            }else if (porcentaje > 0 && porcentaje < 50 && botones.b == true && !entro){
                    filtered.push(el);
                    entro = true;

            }else if ((porcentaje == undefined || porcentaje == 0) && botones.a == true && !entro){
                    filtered.push(el);
                    entro = true;
            }
        });
        return filtered;
    }else{
        return items;
    }
  }
})
.filter('randomSrc', function () {
  return function (input) {
    if (input)
      return input + '?r=' + (new Date()).getTime();
  }
});

