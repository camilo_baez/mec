angular.module('app.services', [])
.service('ConfigService', function($cordovaNetwork, $timeout){
	var service = {};
	//var SERVER_URL = "http://192.168.0.24:8080/fiscalizaciones_server/rest/";
	//var SERVER_URL = "http://181.120.125.139:8080/fiscalizaciones_server/rest/";
	//var SERVER_URL = "http://192.168.0.17:8080/fiscalizaciones_server/rest/";
  service.SERVER_URL = "http://181.122.120.58:8080/fiscalizaciones_server/rest/";
  //var SERVER_URL = "https://mec.gov.py/fiscalizaciones/rest/";
	//service.SERVER_URL ="https://mec.gov.py/fiscalizaciones/rest/";
	service.verificarConexion = function(funcion, mensajeDialogo, tituloDialogo){
		var mensaje = mensajeDialogo || 'Verificar conexión a internet';
		var titulo = tituloDialogo || 'Error de conexión';
		var isOnline = true;
    	if(window.cordova){
    		isOnline = $cordovaNetwork.isOnline();
    	}

    	if(isOnline){
    		funcion();
    	}else{
    		if(window.cordova){
    			$timeout(function(){
        			$cordovaDialogs.alert(mensaje, titulo);
        		}, 100);
		    }else{//Solo para pruebas
		    	$timeout(function(){
        			alert(titulo+"\n\n"+mensaje);
        		}, 100);
		    }
    	}
    	return isOnline;
	}
	return service;
})
.factory('httpRequestInterceptor', function ($localStorage) {
  return {
    request: function (config) {
    	/*
	    config.headers['Authorization'] = 'Basic d2VudHdvcnRobWFuOkNoYW5nZV9tZQ==';
	    config.headers['Accept'] = 'application/json;odata=verbose';
		*/
		if(config.url.match('/rest/') && !config.url.match('login')){
			config.headers['username'] = $localStorage.usernameActivo;
			config.headers['password'] = $localStorage.passwordActivo;
		}
		//config.timeout = 60000;
      return config;
    }
  };
})
.factory('PDFFactory', function($http, $q, $localStorage, $cordovaFileTransfer, $cordovaFile, $cordovaInAppBrowser,
	$cordovaSpinnerDialog, toastr, $cordovaFileOpener2, $cordovaDialogs, $ionicListDelegate){
	//var SERVER_URL = "http://192.168.0.24:8080/fiscalizaciones_server/rest/";
	var factory = {};
	factory.downloadPDF = function(url){
	  console.log('descargando pdf: '+ url);
	 	if(ionic.Platform.isAndroid()){
      		downloadPDFAndroid(url);
	    }else if(ionic.Platform.isIOS()){
	      	openPDFiOS(url);
	    }
	 }

	 var downloadPDFAndroid = function(url) {

	 	if(url.indexOf("manual") >= 0){
	 		$cordovaFile.copyFile(cordova.file.applicationDirectory + "www/", url,
	 			cordova.file.externalCacheDirectory, "manual.pdf").then(function(success) {
	 				openPDFAndroid(this.window.Ionic.WebView.convertFileSrc(success.nativeURL));
				}, function(error) {
					$cordovaDialogs.alert('No se pudo abrir el archivo', 'Error', 'OK')
					    .then(function() {
					    });
				});
	 	}else{
	 		fileTransfer = new FileTransfer();
	        fileTransfer.download(
				url,
	            //cordova.file.externalCacheDirectry + "pdf.pdf",
	            cordova.file.externalCacheDirectory + "documento.pdf",
	            function(entry) {
	                openPDFAndroid(entry.toURL());
	            },
	            function(error) {
	            	ocultarDiaogo();
	            	console.log(angular.toJson(error, true));
	            	if(error.code == 3){//Error de conexion
						$cordovaDialogs.alert('Verificar conexión a internet', 'Error', 'OK')
					    .then(function() {
					      $ionicListDelegate.closeOptionButtons();
					    });
	            	}else{
	            		$cordovaDialogs.alert('No se pudo descargar el archivo o no existe', 'Error', 'OK')
					    .then(function() {
					      $ionicListDelegate.closeOptionButtons();
					    });
	            	}
	            },
	            false,
	            null
	        );
	        $cordovaSpinnerDialog.show("Descargando","Espere un momento por favor", true);
	 	}
	}
	var openPDFAndroid = function(filePath) {
	    $cordovaFileOpener2.open(
	        filePath, // Any system location, you CAN'T use your appliaction assets folder
	        'application/pdf'
	    ).then(function(suc) {
	    	ocultarDiaogo();
	        $ionicListDelegate.closeOptionButtons();
	    }, function(err) {
	    	ocultarDiaogo();
	    	if(err.status == 9){//Si no encuentra un activity que pueda manejar el PDF
	    		console.log(angular.toJson(err, true));
	    		$cordovaDialogs.alert('Debe tener instalada una aplicación para abrir archivos PDF', 'Error', 'OK')
			    .then(function() {
			      $ionicListDelegate.closeOptionButtons();
			    });
	    	}else{
	    		$cordovaDialogs.alert('No se puede mostrar el archivo', 'Error', 'OK')
			    .then(function() {
			      $ionicListDelegate.closeOptionButtons();
			    });
	    	}
	    });
	};
	var openPDFiOS = function(filePath) {
		console.log("download ios");
		 var options = {
	      location: 'no',
	      clearcache: 'yes',
	      toolbar: 'yes',
	      closebuttoncaption: 'OK',
	      enableViewportScale: 'yes'
	    };
		$cordovaInAppBrowser.open(filePath, '_blank', options)
	      .then(function(event) {
	      	ocultarDiaogo();
	        $ionicListDelegate.closeOptionButtons();
	      })
	      .catch(function(event) {
	      	ocultarDiaogo();
	      	$cordovaDialogs.alert('No se puede mostrar el archivo', 'Error', 'OK')
			    .then(function() {
			      $ionicListDelegate.closeOptionButtons();
			    });
	      });
	      $cordovaSpinnerDialog.show("Descargando","Espere un momento por favor", true);
	};
	var ocultarDiaogo = function(){
	 	$cordovaSpinnerDialog.hide();
	 };
	return factory;
})
.factory('DatosFactory', function($http, $q, $rootScope, $localStorage, $cordovaFileTransfer, $state, $cordovaNetwork,
	toastr, $cordovaDialogs, $cordovaSpinnerDialog, $ionicSideMenuDelegate, ConfigService, FotosService, $ionicListDelegate){

	var SERVER_URL = ConfigService.SERVER_URL;

	var factory = {};

	var canceler = $q.defer();

	factory.setScope = function(myScope){
		if(myScope)
			factory.scope = myScope;
	}
	var background = false;
	factory.getDatos = function(inBackground){
		background = inBackground || false;
		var peticion = $http({
			method: 'GET',
			url: SERVER_URL + 'fiscalizaciones'
		});

		//if(!background){
			if (window.cordova){
				$cordovaSpinnerDialog.show("Descargando","Actualizando. Espere un momento por favor", true);
			}
		//}else{
		//	console.log('background');
		//}
		peticion.success(function(data) {
		  console.log(angular.toJson(data, true));
    		if(data){
    			delete $localStorage.locales;
          delete $localStorage.localGlobal;
          delete $localStorage.obraGlobal;
          delete $localStorage.fiscalizacionGlobal;

          $localStorage.id_avance = 0;

          $localStorage.locales = data.locales;
          console.log($localStorage.locales);
          $localStorage.situaciones = data.situaciones;
          $localStorage.tipos_verificaciones = data.tipos_verificaciones;
            if(factory.scope){
              factory.scope.locales = data.locales;
            }else{
              $rootScope.$broadcast('locales:updated',1);
            }
          $state.go('menu.locales');

          FotosService.vaciarDirectorioFotos();
    		}else{
	            if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard){
	            	cordova.plugins.Keyboard.close();
	        	}
	        	if(window.cordova){
					$cordovaDialogs.alert(data.mensaje, 'Error');
			    }else{//Solo para pruebas
			    	alert('Error\n ' + data.mensaje);
			    }
   			}
   			if (window.cordova){
          $cordovaSpinnerDialog.hide();
        }
        if(!background){
          $ionicSideMenuDelegate.toggleLeft();
        }
    }).error(function(error) {
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard){
        cordova.plugins.Keyboard.close();
      }
   			//if(!background){
			if (window.cordova){
				$cordovaSpinnerDialog.hide();
			}
			console.log(angular.toJson(error, true));
			dialogoErrorConexion();
   			//}
		});
	};

	var dialogoErrorConexion = function(){
		if(!background){
			if(window.cordova){
			$cordovaDialogs.alert('Error de conexión', 'Error');
		    }else{//Solo para pruebas
		    	alert('Error\nError de conexión');
		    }
		}
	};

	factory.guardarFiscalizacion = function(fiscalizacion){
		//console.log(JSON.stringify(fisca));
		var datos = {};
		datos.usuario = $localStorage.usernameActivo;
		datos.contrasena = $localStorage.passwordActivo;
		datos.fiscalizacion = fiscalizacion;
		return $http({

	  method: 'POST', data: fiscalizacion,
		url: SERVER_URL + 'fiscalizaciones/admin/admin',
		headers: {'Content-Type': 'application/json'}
	  })
	};

	factory.getDatosLocales = function(){
		return $http.get('./data/locales.json');
	};

  factory.getTiposVerificacionesLocales = function(){
    return $http.get('./data/tipos_verificaciones.json');
  };

	factory.loginLocal = function(user, pass){
		if(user == "admin"){
			return $http.get('./data/fiscalizador.json');
		}else{
			return $http.get('./data/fiscalizador2.json');
		}
	};

	factory.login = function(user, pass){
		return $http({

	  method: 'POST', data: {username: user, password: pass},
	  transformRequest: function(obj) {
        var str = [];
        for(var p in obj)
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
        return str.join("&");
    	},

		url: SERVER_URL + 'login',
	  //url: 'http://localhost:8080/fiscalizaciones_server/rest/login',
	  headers: {'Content-Type': 'application/x-www-form-urlencoded'}})
	};

	factory.cancel = function(){
		canceler.resolve();
	};

	factory.getPendientes = function(){
		var pendientes = [];
		angular.forEach($localStorage.locales, function(local) {
			angular.forEach(local.obras, function(obra) {
				angular.forEach(obra.fiscalizaciones, function(fiscalizacion) {
					if(fiscalizacion.estado){
						pendientes.push(fiscalizacion);
					}
				});
			});
		});
		return pendientes;
	}
	factory.contarPendientes = function(){
		var cantidad = 0;
		angular.forEach($localStorage.locales, function(local) {
			angular.forEach(local.obras, function(obra) {
				angular.forEach(obra.fiscalizaciones, function(fiscalizacion) {
					if(fiscalizacion.estado){
						cantidad = cantidad + 1;
					}
				});
			});
		});
		return cantidad;
	}
	factory.contarPendientesLocal = function(local){
		var cantidad = 0;
		angular.forEach(local.obras, function(obra) {
			angular.forEach(obra.fiscalizaciones, function(fiscalizacion) {
				if(fiscalizacion.estado){
					cantidad = cantidad + 1;
				}
			});
		});
		return cantidad;
	}
	factory.contarPendientesObra = function(obra){
		var cantidad = 0;
		angular.forEach(obra.fiscalizaciones, function(fiscalizacion) {
			if(fiscalizacion.estado){
				cantidad = cantidad + 1;
			}
		});
		return cantidad;
	}

	factory.tienePendientes = function(){
		return factory.getPendientes().length > 0;
	}

	//TODO: verificar si no tiene pendienes, no deberia intentar enviar
	var enviarFiscalizaciones = function(pendientes, individual) {
		var fotos = []; //Array de todas las imagenes a enviar
		var defs = [];
		var fd = new FormData();
		var fiscalizacionGlobal = {};
		var nombreImagen = "";

		var i = 0;
    var borradas = [];
    pendientes.forEach(function(fiscalizacion){
      borradas = [];
      console.log(angular.toJson(fiscalizacion.observaciones, true));

      //Quita las barras a la fecha para utilizar como identificador
			var id_obra = fiscalizacion.id_obra;
			var id_avance = fiscalizacion.id_avance;
			fotos = fiscalizacion.anexos;
			if(fotos){
				//console.log("Preparando "+fotos.length+" imagenes.");
				fotos.forEach(function(foto) {
					//console.log('procesando: '+ angular.toJson(foto, true));
					var def = $q.defer();
					if(foto.src && foto.id_anexo < 0){
					  console.log(foto.src);

            FotosService.getFoto(id_avance, foto.id_anexo).then(function (success) {
              console.log('blob agregado al formdata');
              var imgBlob = new Blob([success], { type:'image/jpg'});
              console.log('file_'+id_obra + '_' + id_avance + "/" + foto.id_anexo);
              fd.append('file_' + id_obra + '_' + id_avance, imgBlob, foto.id_anexo);
              def.resolve();
            }, function (error) {
              console.log('error getting file', error);
              def.reject();
            });
            /*
						window.resolveLocalFileSystemURL(foto.src, function(fileEntry) {
							fileEntry.file(function(file) {
								var reader = new FileReader();
								reader.onloadend = function(e) {
								  console.log('result:' + this.result);
									var imgBlob = new Blob([this.result], { type:file.type});
									//console.log('file_'+id_obra + '_' + id_avance + "/" + foto.id_anexo);
									fd.append('file_' + id_obra + '_' + id_avance, imgBlob, foto.id_anexo);
									def.resolve();
								};
								reader.readAsArrayBuffer(file);

							}, function(e) {
								console.log('error getting file', e);
								def.reject();
							});
						}, function(e) {
              console.log('Error resolving fs url ' + angular.toJson(e, true));
              console.log('Error resolving fs url ' + e);
							def.reject();

						});
            */
						defs.push(def.promise);
					}
				});
			}
			if(fiscalizacion.borradas){
			  if(fiscalizacion.borradas.length > 0){
          angular.forEach(fiscalizacion.borradas , function(id_anexo) {
              borradas.push(id_anexo);
          });
        }
      }
      //Se agregan todos los id de fotos borradas
      if(borradas.length > 0){
			  console.log('fotos borradas agregadas al formdata');
        fd.append("borradas", angular.toJson(borradas));
      }

			var defFirmaFiscalizador = $q.defer();
			var defFirmaContratista = $q.defer();
			FotosService.getFile(fiscalizacion.id_avance, 'firmaFiscalizador')
			      .then(function (success) {
			      	console.log('firma fiscalizador agregada al formdata');
			    	var firmaBlob = new Blob([success], { type:'image/png'});
					fd.append('file_' + id_obra + '_' + id_avance, firmaBlob, 'fiscalizador');
					defFirmaFiscalizador.resolve();
			      }, function (error) {
			      	defFirmaFiscalizador.reject();
			 });
	       	FotosService.getFile(fiscalizacion.id_avance, 'firmaContratista')
	        	.then(function (success) {
	        		console.log('firma contratista agregada al formdata');
			      	var firmaBlob = new Blob([success], { type:'image/png'});
					fd.append('file_' + id_obra + '_' + id_avance, firmaBlob, 'contratista');
					defFirmaContratista.resolve();
			      }, function (error) {
			      	defFirmaContratista.reject();
			});
	        defs.push(defFirmaFiscalizador.promise);
	        defs.push(defFirmaContratista.promise);
		});

		var defFiscalizaciones = $q.defer();

		//Luego de procesar todas las fotos, se agregan las fiscalizaciones
    console.log('verificando fotos y firma');
		$q.all(defs).then(function() {
			console.log("todas las imagenes se cargaron al listado para ser enviados");
			/*
			if(individual){
        var uri = encodeURI(SERVER_URL + "fiscalizaciones/obra/"+ pendientes[0].id_obra);
      }else{
        var uri = encodeURI(SERVER_URL + "fiscalizaciones");
      }
      */
      var uri = encodeURI(SERVER_URL + "fiscalizaciones");
			fd.append("fiscalizaciones", angular.toJson(pendientes));
			$http({
			  method: 'PUT', data: fd,
				url: uri,
				headers: {'Content-Type': undefined}
			  }).then(function (success) {
			  	defFiscalizaciones.resolve(success.data);
			}, function (error) {
				defFiscalizaciones.reject(error);
			});
		})
    .catch(function(error) {
      console.log("error al agregar un campo al formdata: " + angular.toJson(error, true));
      defFiscalizaciones.reject(error);
    });
		return defFiscalizaciones.promise;
	}

  var enviar = function(pendientes, individualParametro){
    var individual = individualParametro || false;

    $cordovaSpinnerDialog.show("Enviando","Espere un momento por favor", true);
    enviarFiscalizaciones(pendientes, individual).then(function (success) {
      var fiscalizacionesActualizadas = success;
      FotosService.moverFotos(fiscalizacionesActualizadas);
      //console.log(angular.toJson(fiscalizacionesActualizadas, true));

      if(individual) {
        $localStorage.obraGlobal.fiscalizaciones = angular.copy(fiscalizacionesActualizadas);
        $rootScope.$broadcast('fiscalizaciones:updated',1);
        /*
        angular.forEach(fiscalizacionesActualizadas , function(actualizada) {
          console.log(angular.toJson(actualizada, true));

          //Si alguna fiscalizacion no concuerda en id_avance, entonces es una nueva fiscalizacion
          //generada en el servidor

          //Si tiene id_logico, es una nueva fiscalización generada en la app
          if(actualizada.id_logico){

          }
          //Si el id_avance se encuentra en el listado de fiscalizaciones, entonces se debe actualizar

          //Si el id_avance no se encuentra en el listado de fiscalizaciones, entonces es una
          // fiscalizacion generada en el servidor y se debe agregar a la app

          for(i in $localStorage.obraGlobal.fiscalizaciones){
            var actual = $localStorage.obraGlobal.fiscalizaciones[i];
            if(actualizada.id_logico){//nueva
              if(actualizada.id_logico == actual.id_avance){
                $localStorage.obraGlobal.fiscalizaciones[i] = angular.copy(actualizada);
              }
            }else{
              if(actualizada.id_avance == actual.id_avance){
                $localStorage.obraGlobal.fiscalizaciones[i] = angular.copy(actualizada);
              }
            }
          };
        });
        */
      }else{

        for(i in $localStorage.locales){
          for(j in $localStorage.locales[i].obras){
            var fiscalizaciones = [];
            angular.forEach(fiscalizacionesActualizadas, function(fiscalizacion) {
              if($localStorage.locales[i].obras[j].id_obra == fiscalizacion.id_obra){
                fiscalizaciones.push(fiscalizacion);
              }
            });
            $localStorage.locales[i].obras[j].fiscalizaciones = fiscalizaciones;
          }
        }
          /*
          angular.forEach($localStorage.locales, function(local) {
          angular.forEach(local.obras, function(obra) {
            var fiscalizaciones = [];
            angular.forEach(fiscalizacionesActualizadas, function(fiscalizacion) {
                if(obra.id_obra == fiscalizacion.id_obra){
                  fiscalizaciones.push(fiscalizacion);
                }
            });
            obra.fiscalizaciones = fiscalizaciones;
          });
        });
        */
        $rootScope.$broadcast('fiscalizacionesTotal:updated',1);
      }

      ocultarDiaogo();
    }, function (response) {
      console.log(angular.toJson(response, true));
      if(window.cordova){
        if(response.data){
          $cordovaDialogs.alert(response.data.detail,response.data.error);
        }else{
          $cordovaDialogs.alert('Verificar datos','Error al enviar');
        }
      }else{//Solo para pruebas
        alert('Error al enviar\nVerificar datos');
      }
      ocultarDiaogo();
    });
  };

  factory.enviarPendientes = function(fiscalizaciones, individual) {
    verificarConexion(function(){
      procesarEnvio(fiscalizaciones, individual)});
  };

	var procesarEnvio = function (fiscalizaciones, individual) {
    if(window.cordova){
      $cordovaDialogs.confirm('¿Está seguro que quiere enviar las fiscalizaciones pendientes?','Enviar',
        ['Aceptar','Cancelar'])
        .then(function(buttonIndex) {
          if(buttonIndex == 1){
            enviar(fiscalizaciones, individual);
          }
        });
    }else{//Solo para pruebas
      var ok = confirm('Está seguro que quiere enviar las fiscalizaciones pendientes?');
      if(ok){
        enviar(fiscalizaciones, individual);
      }
    }
  };

	factory.generarCertificado = function (fiscalizacion) {
     verificarConexion(function(){
       generarCertificado(fiscalizacion)
     });
  }

  var generarCertificado = function (fiscalizacion) {
    $cordovaSpinnerDialog.show("Generando","Espere un momento por favor", true);
    var uri = SERVER_URL + 'fiscalizaciones/' + fiscalizacion.id_avance + '/certificado';
    $http({
      method: 'PUT',
      url: uri,
      headers: {'Content-Type': undefined}
    }).then(function (success) {
      console.log('************** Certificado generado: '+JSON.stringify(success));
      fiscalizacion.numero_certificado = success.data.numero_certificado;
      $ionicListDelegate.closeOptionButtons();
      $rootScope.$broadcast('certificado:updated',1);
      ocultarDiaogo();
    }, function (error) {
      console.log('************** Certificado error: '+JSON.stringify(error));
      $ionicListDelegate.closeOptionButtons();
      $rootScope.$broadcast('certificado:updated',1);
      ocultarDiaogo();
      //alert(JSON.stringify(error));
    });
  }

  var verificarConexion = function(funcion){
    var isOnline = true;
    if(window.cordova){
      isOnline = $cordovaNetwork.isOnline();
    }
    if(isOnline){
      console.log('hay conexion');
      funcion();
    }else{
      if(window.cordova){
        $cordovaDialogs.alert('Verificar conexión a internet','Error de conexión');
      }else{//Solo para pruebas
        alert('Verificar conexión a internet');
      }
    }
  };

  var ocultarDiaogo = function(){
    $cordovaSpinnerDialog.hide();
  };

	return factory;
})

.service('FotosService', function($q, $cordovaFile, $http, $cordovaFileTransfer,$localStorage, $ionicPlatform, ConfigService){
	var SERVER_URL = ConfigService.SERVER_URL;
	var factory = {};
	factory.directorioFotos = "fotos";
	var directorioAPP = "";

	factory.guardarFotos = function(fotos, subDirectorio){
		var numeroFoto = 0;

		angular.forEach(fotos , function(foto) {
			if(foto.id_anexo < numeroFoto){
				numeroFoto = foto.id_anexo;
			}
			foto.id_avance = subDirectorio;
			console.log("----"+ foto.id_avance);
		});
		numeroFoto = numeroFoto - 1;

		var promises = [];

		angular.forEach(fotos , function(foto) {
			var sourcePath = "";
			var sourceDirectory = "";
			var sourceFileName = "";
			var nombreFinal = "";
			var destino = "";
			var directorioFinal = factory.directorioFotos + "/" + subDirectorio;
			if(foto.temp != undefined){
				var deffered  = $q.defer();
		    	sourcePath = foto.temp;

				sourceDirectory = sourcePath.substring(0, sourcePath.lastIndexOf('/') + 1);
				sourceFileName = sourcePath.substring(sourcePath.lastIndexOf('/') + 1, sourcePath.length);

				nombreFinal = numeroFoto + ".jpg";
				destino = directorioFinal + "/" + nombreFinal;

				foto.id_anexo = numeroFoto;

				factory.borrarFoto(subDirectorio, numeroFoto).then(function (success) {
					copiarFotos(sourceDirectory, sourceFileName, directorioAPP, destino, foto, deffered);
			      }, function (error) {
					copiarFotos(sourceDirectory, sourceFileName, directorioAPP, destino, foto, deffered);
			      });
				numeroFoto = numeroFoto - 1;
		        promises.push(deffered.promise);
			}
	    });
		return $q.all(promises);
	}
	var copiarFotos = function(sourceDirectory, sourceFileName, directorioAPP, destino, foto, deffered){
	  console.log('sourceDirectory: ' + angular.toJson(sourceDirectory, true));
    console.log('sourceFileName: '+ angular.toJson(sourceFileName, true));
    console.log('directorioAPP: '+ angular.toJson(directorioAPP, true));
    console.log('destino: '+ angular.toJson(destino, true));
		$cordovaFile.moveFile(sourceDirectory, sourceFileName, directorioAPP, destino).then(function(success) {
			foto.src = this.window.Ionic.WebView.convertFileSrc(success.nativeURL);
			foto.temp = undefined;
      foto.tempSanitized = undefined;
			console.log('Foto guardada en : '+ foto.src);
			deffered.resolve(success);

		}, function(error) {
			deffered.reject(error);
      console.log(angular.toJson(error, true));
      console.log('error: ' + directorioAPP + destino);
			/*
			if(error.code == 12){
				console.log('Foto existe en : '+ directorioAPP + destino);
				deffered.resolve(1);
			}else{
				foto.estado = 'error';
				console.log('No se pudo guardar la Foto: '+ angular.toJson(error, true));

				deffered.reject(error);
			}
			*/
		});

	}

	factory.copiarFotoTemp = function (foto, scope) {
    var subDirectorio = "temp";
    var directorioFinal = directorioAPP + factory.directorioFotos + "/" + subDirectorio;
    var sourcePath = foto.temp;
    var sourceDirectory = sourcePath.substring(0, sourcePath.lastIndexOf('/') + 1);
    var sourceFileName = sourcePath.substring(sourcePath.lastIndexOf('/') + 1, sourcePath.length);
    console.log('copiando: ' + angular.toJson(directorioFinal, true));
    console.log('copiando sourcePath: ' + angular.toJson(sourcePath, true));

    $cordovaFile.moveFile(sourceDirectory, sourceFileName, directorioFinal).then(function(success) {
      console.log('copiado con exito:');
      foto.temp = success.nativeURL;
      foto.tempSanitized = this.window.Ionic.WebView.convertFileSrc(success.nativeURL);
      if(scope.allImages){
        scope.allImages.splice(0,0,foto);
      }else{
        scope.fiscalizacion.anexos = [];
        scope.allImages = scope.fiscalizacion.anexos;
        scope.allImages.push(foto);
      }
    }, function(error) {
      console.log('error: ' + angular.toJson(error, true));
    });
  }

  var copiarFotoTemp = function(sourceDirectory, sourceFileName, directorioAPP, destino){
    $cordovaFile.moveFile(sourceDirectory, sourceFileName, directorioAPP, destino).then(function(success) {
      foto.src = this.window.Ionic.WebView.convertFileSrc(success.nativeURL);
      foto.temp = undefined;
      foto.tempSanitized = undefined;
      console.log('Foto guardada en : '+ foto.src);
      deffered.resolve(success);

    }, function(error) {
      deffered.reject(error);
      console.log('error: ' + directorioAPP + destino);
      /*
       if(error.code == 12){
       console.log('Foto existe en : '+ directorioAPP + destino);
       deffered.resolve(1);
       }else{
       foto.estado = 'error';
       console.log('No se pudo guardar la Foto: '+ angular.toJson(error, true));

       deffered.reject(error);
       }
       */
    });

  }

	factory.borrarFoto = function (id_avance, id_anexo) {
		if(window.cordova){
			var subDirectorio = id_avance;
			var nombreFinal = id_anexo + extensionArchivo;
			var directorioArhcivo = factory.directorioFotos + "/" + subDirectorio;
			var pathDispositivo = directorioAPP + directorioArhcivo + "/" + nombreFinal;
			console.log('Borrando foto: ' + pathDispositivo);
			return $cordovaFile.removeFile(directorioAPP, directorioArhcivo + "/" + nombreFinal);
		}
	}
	factory.borrarDirectorioFiscalizacion = function (fiscalizacion) {
		if(window.cordova){
			var subDirectorio = fiscalizacion.id_avance;
			var directorioArhcivo = factory.directorioFotos + "/" + subDirectorio;
			console.log('Borrando directorio: ' + directorioArhcivo);
      clearDirectory(directorioArhcivo);
      /*
      $cordovaFile.removeDir(directorioAPP, directorioArhcivo ).then(function (success) {
				console.log('Borrado');
		      }, function (error) {
		      	console.log('error');
		      });

		      */
		}
	}

  factory.vaciarDirectorioFotos = function () {
    clearDirectory('fotos/');
    /*
    if(window.cordova){
      var directorioArhcivo = factory.directorioFotos;
      console.log('Borrando directorio: '+directorioAPP + directorioArhcivo);
      $cordovaFile.removeDir(directorioAPP, directorioArhcivo ).then(function (success) {
        console.log('Borrado');
        factory.crearDirectorioFotos();
      }, function (error) {
        console.log('error: '+ angular.toJson(error,true) );
        factory.crearDirectorioFotos();
      });
    }
    */
  }

  factory.vaciarDirectorioTemp = function () {
    if(window.cordova){
      var directorioArhcivo = factory.directorioFotos + "/temp";
      console.log('Borrando directorio: '+directorioAPP + directorioArhcivo);
      $cordovaFile.removeDir(directorioAPP, directorioArhcivo ).then(function (success) {
        console.log('Borrado');
        factory.crearDirectorioFotosTemp();
      }, function (error) {
        console.log('error');
        factory.crearDirectorioFotosTemp();
      });
    }
  }

  function clearDirectory(subDir) {
    var subDirectorio = subDir || '';
     window.resolveLocalFileSystemURL(directorioAPP + subDirectorio, onFileSystemDirSuccess, fail);
  };

  function onFileSystemDirSuccess(fileSystem) {
    var entry = "";
    entry = fileSystem;

    entry.removeRecursively(function() {
      console.log("Delete successful !!!");
      factory.crearDirectorioFotos();
    }, fail);
  }

  function getDirFail(error) {
    console.log("getDirFail - " + angular.toJson(error,true));
  }

  function fail(error) {
    console.log("fail - " + angular.toJson(error,true));
  }

	factory.descargarFoto = function ($scope, image, fiscalizacion, nombreFinal, forzarDescarga) {
    ConfigService.verificarConexion(function(){
      descargarFoto($scope, image, fiscalizacion, nombreFinal, forzarDescarga)
    });
  };

  var descargarFoto = function ($scope, image, fiscalizacion, nombreFinal, forzarDescarga) {
		var forzar = forzarDescarga || false;
		var subDirectorio = fiscalizacion.id_avance;
		var uri = SERVER_URL + 'fiscalizaciones/anexos/' + image.id_anexo;
    console.log('id_anexo: ' + image.id_anexo);
    if(image.id_anexo && image.id_anexo > 0){
		  if(window.cordova){
				//Primero se debe verificar si ya está en el dispositivo
				var directorioArhcivo = factory.directorioFotos + "/" + subDirectorio;
				var pathDispositivo = directorioAPP + directorioArhcivo + "/" + nombreFinal;
				console.log('Verificando archivo: ' + nombreFinal);
				if(forzar){
					descargar(uri, pathDispositivo, $scope, image);
				}else{
          console.log("IMAGE SRC: " + image.src );
					$cordovaFile.checkFile(directorioAPP, directorioArhcivo + "/" + nombreFinal).then(function (success) {
						//Si la imagen ya se encuentra en el dispositivo, entonces se asigna el path
	        			image.src = this.window.Ionic.WebView.convertFileSrc(pathDispositivo);
	        			console.log("SE ENCONTRO: " + image.src );

				      }, function (error) {
				      	//Si la imagen no se encuentra en el dispositivo, entonces se descarga
				        descargar(uri, pathDispositivo, $scope, image);
				      });
				}
			}else{
        image.src = 'img/error_imagen.png';
      }
		}
	    /*
		    1 = FileTransferError.FILE_NOT_FOUND_ERR
		    2 = FileTransferError.INVALID_URL_ERR
		    3 = FileTransferError.CONNECTION_ERR
		    4 = FileTransferError.ABORT_ERR
		    5 = FileTransferError.NOT_MODIFIED_ERR
	    */
	}

	var descargar = function(uri, pathDispositivo, $scope, image){
		var fileTransfer = new FileTransfer();
	  var path = pathDispositivo;
    var puedeGuardar = false;
    console.log('verificando directorio: fotos/'+ $scope.fiscalizacion.id_avance);
    factory.crearSubDirectorioFotos($scope.fiscalizacion.id_avance).then(function (success) {
        puedeGuardar = true;
      }).catch(function(response) {
        if(response.code == 12){//Solo si el directorio ya existe
          puedeGuardar = true;
        }
      }).finally(function() {
        if(puedeGuardar){
          var anterior = image.src;
          image.src = 'img/cargando.gif';
          console.log('descargando de: ' +uri);
          fileTransfer.download(
            uri, //cordova.file.dataDirectory/fotos/1/1.pdf
            path,
            //path+ "_" + (new Date()).getTime(),
            function (entry) {
              console.log("descarga completa: " + angular.toJson(uri, true));
              //image.src = entry.toURL();
              $scope.$apply(function () {
                image.src = this.window.Ionic.WebView.convertFileSrc(entry.toURL());
              });

              //readFile(entry);
            },
            function (error) {
              console.log("descarga error: " + JSON.stringify(error));
              $scope.$apply(function () {
                image.src = 'img/error_imagen.png';
              });
            },
            true, // or, pass false
            {
              headers: {
                  "username": $localStorage.usernameActivo,
                  "password": $localStorage.passwordActivo
              }
            }
          );
        }
      });
	}

  factory.descargarFirma = function ($scope, image, fiscalizacion, nombreFinal, forzarDescarga) {
    ConfigService.verificarConexion(function(){
      descargarFirma($scope, image, fiscalizacion, nombreFinal, forzarDescarga);
    });
  };

  var descargarFirma = function ($scope, image, fiscalizacion, nombreFinal, forzarDescarga) {
    var forzar = forzarDescarga || false;
    var subDirectorio = fiscalizacion.id_avance;
    var pathDispositivo = "";
    var uri = SERVER_URL + 'fiscalizaciones/anexos/' + image.id_anexo;
      if(window.cordova){
        //Primero se debe verificar si ya está en el dispositivo
        var directorioArhcivo = factory.directorioFotos + "/" + subDirectorio;
        pathDispositivo = directorioAPP + directorioArhcivo + "/" + nombreFinal;
        console.log('Verificando archivo: ' + nombreFinal);
        if(forzar){
          descargarFirmaFinal(uri, pathDispositivo, $scope, image);
        }else{
          console.log("IMAGE FIRMA SRC: " + image.src );

          $cordovaFile.checkFile(directorioAPP, directorioArhcivo + "/" + nombreFinal).then(function (success) {
            //Si la imagen ya se encuentra en el dispositivo, entonces se asigna el path
            image.src = pathDispositivo;
            if(nombreFinal == 'firmaFiscalizador.png'){
              factory.getFile($scope.fiscalizacion.id_avance, 'firmaFiscalizador')
                .then(function (success) {
                  $scope.firma = success;
                  localStorage.setItem('firma', $scope.firma);
                }, function (error) {
                  console.log(error);

                });
            }
            if(nombreFinal == 'firmaContratista.png'){
              factory.getFile($scope.fiscalizacion.id_avance, 'firmaContratista')
                .then(function (success) {
                  $scope.firmaContratista = success;
                  localStorage.setItem('firmaContratista', $scope.firmaContratista);
                }, function (error) {
                  console.log(error);

                });
            }
            console.log("SE ENCONTRO FIRMA: " + image.src );

          }, function (error) {
            //Si la imagen no se encuentra en el dispositivo, entonces se descarga
            descargarFirmaFinal(uri, pathDispositivo, $scope, image);
          });

        }
      }else{
        image.src = 'img/error_imagen.png';
      }

    /*
     1 = FileTransferError.FILE_NOT_FOUND_ERR
     2 = FileTransferError.INVALID_URL_ERR
     3 = FileTransferError.CONNECTION_ERR
     4 = FileTransferError.ABORT_ERR
     5 = FileTransferError.NOT_MODIFIED_ERR
     */
  }

  var descargarFirmaFinal = function(uri, pathDispositivo, $scope, src){
    var fileTransfer = new FileTransfer();
    var path = pathDispositivo;
    var puedeGuardar = false;
    console.log('verificando directorio: fotos/'+ $scope.fiscalizacion.id_avance);
    factory.crearSubDirectorioFotos($scope.fiscalizacion.id_avance).then(function (success) {
      puedeGuardar = true;
    }).catch(function(response) {
      if(response.code == 12){//Solo si el directorio ya existe
        puedeGuardar = true;
      }
    }).finally(function() {
      if(puedeGuardar){
        console.log('descargando de: ' +uri);
        fileTransfer.download(
          uri, //cordova.file.dataDirectory/fotos/1/1.pdf
          path,
          //path+ "_" + (new Date()).getTime(),
          function (entry) {
            console.log("descarga completa: " + angular.toJson(uri, true));
            //image.src = entry.toURL();
            if(pathDispositivo.includes("firmaFiscalizador")){
              factory.getFile($scope.fiscalizacion.id_avance, 'firmaFiscalizador')
                .then(function (success) {
                    $scope.firma = success;
                    localStorage.setItem('firma', $scope.firma);
                }, function (error) {
                });
            }
            if(pathDispositivo.includes("firmaContratista")){
              factory.getFile($scope.fiscalizacion.id_avance, 'firmaContratista')
                .then(function (success) {
                    $scope.firmaContratista = success;
                    localStorage.setItem('firmaContratista', $scope.firmaContratista);
                }, function (error) {
                });
            }

            //readFile(entry);
          },
          function (error) {
            console.log("descarga error: " + JSON.stringify(error));
          },
          true, // or, pass false
          {
            headers: {
              "username": $localStorage.usernameActivo,
              "password": $localStorage.passwordActivo
            }
          }
        );
      }
    });
  }

	factory.crearDirectorioFotos = function(){
		$cordovaFile.createDir(directorioAPP, factory.directorioFotos, false)
		.then(function (success) {
			console.log('Directorio de Fotos creado');
      factory.crearDirectorioFotosTemp();
			//console.log(JSON.stringify(success));
		}, function (error) {
			console.log('Error al crear directorio de Fotos: ' + error.message);
		});
	}
  factory.crearDirectorioFotosTemp = function(){
    $cordovaFile.createDir(directorioAPP, factory.directorioFotos + "/temp", false)
      .then(function (success) {
        console.log('Directorio de Fotos TEMP creado');
        //console.log(JSON.stringify(success));
      }, function (error) {
        console.log('Error al crear directorio de Fotos TEMP: error.' + error.message);
        console.log(JSON.stringify(error));
      });
  }

	factory.crearSubDirectorioFotos = function(subDirectorio){
		return $cordovaFile.createDir(directorioAPP, factory.directorioFotos + "/" + subDirectorio, false);
	}

	var extensionArchivo = '.jpg';
	factory.moverFotos = function(fiscalizacionesActualizadas){
		fiscalizacionesActualizadas.forEach(function(fiscalizacionNueva) {
        	if(fiscalizacionNueva.anexos){
        		var anexosNuevos = fiscalizacionNueva.anexos;
        		anexosNuevos.forEach(function(anexoNuevo) {
        		    if(anexoNuevo.id_logico){
                  moverFoto(fiscalizacionNueva, anexoNuevo);
                }
        		});
        	}
        });
	}
	var moverFoto = function(fiscalizacionNueva, anexoNuevo){

		if(fiscalizacionNueva.id_logico){
			var directorioArhcivoOrigen = factory.directorioFotos + "/" + fiscalizacionNueva.id_logico;
			var directorioArhcivoDestino = factory.directorioFotos + "/" + fiscalizacionNueva.id_avance;
		}else{
			var directorioArhcivoOrigen = factory.directorioFotos + "/" + fiscalizacionNueva.id_avance;
			var directorioArhcivoDestino = directorioArhcivoOrigen;
		}

		if(window.cordova){
      var puedeGuardar = false;
		  factory.crearSubDirectorioFotos(fiscalizacionNueva.id_avance).then(function (success) {
        puedeGuardar = true;
      }).catch(function(response) {
        if(response.code == 12){//Solo si el directorio ya existe
          puedeGuardar = true;
        }
      }).finally(function() {
        if(puedeGuardar){
          var pathArchivoOrigen = directorioArhcivoOrigen + "/" + anexoNuevo.id_logico + extensionArchivo;
          var pathArchivoDestino = directorioArhcivoDestino + "/" + anexoNuevo.id_anexo + extensionArchivo;
          console.log('moviendo archivo: \nDe: ' + pathArchivoOrigen+ '\nA: '+ pathArchivoDestino);
          $cordovaFile.moveFile(directorioAPP, pathArchivoOrigen, directorioAPP, pathArchivoDestino).then(function (success) {
            //Si la imagen ya se encuentra en el dispositivo, entonces se asigna el path
            fiscalizacionNueva.src = directorioAPP + pathArchivoDestino;
            console.log("MOVIDO A: " + fiscalizacionNueva.src );
          }, function (error) {
            //Si la imagen no se encuentra en el dispositivo, entonces se descarga
            console.log("ERROR: " + angular.toJson(error));
          });
        }
      });
		}else{
	    	fiscalizacionNueva.src = 'img/error_imagen.png';
	    }
	}

	factory.getFile = function(subDirectorio, firma){
		// READ
    console.log();
	    return $cordovaFile.readAsText(directorioAPP + factory.directorioFotos + "/" + subDirectorio, firma + ".png");
	}

	factory.getFoto = function (subDirectorio, id_anexo) {
    return $cordovaFile.readAsArrayBuffer(directorioAPP + factory.directorioFotos + "/" + subDirectorio, id_anexo + ".jpg");
  }

	factory.savefile = function(dataurl, subDirectorio, firma){
			$cordovaFile.writeFile(directorioAPP + factory.directorioFotos + "/" + subDirectorio, firma + ".png", dataurl|| "", true)
		      .then(function (success) {
		        // success
		        console.log('guradado: ' + directorioAPP + factory.directorioFotos + "/" + subDirectorio + firma + ".png");
		      }, function (error) {
		        // error
		        console.log('error al guardar firma');
		        console.log(angular.toJson(error, true));
		      });
		/*
		console.log();
		window.resolveLocalFileSystemURL(directorioAPP + subDirectorio + "/" + firma, function(fileEntry) {
			fileEntry.file(function(file) {
				var reader = new FileReader();
				reader.onloadend = function(e) {
						console.log("Start creating image file");
	                    writer.seek(0);
	                    writer.write(dataurl);
	                    console.log("End creating image file. File created");
				};
				reader.readAsArrayBuffer(file);

			}, function(e) {
				console.log('error getting file', e);
			});
		}, function(e) {
			console.log('Error resolving fs url', e);

		});
		*/

		/*
	    window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,
	    function (fileSystem) {
	        fileSystem.root.getDirectory( "fotos/" + subDirectorio + "/", {create:true, exclusive: false},
	        function(directory) {
	        	console.log(angular.toJson(directory, true));
	            directory.getFile(firma + ".png", {create: true, exclusive: false},
	            function (fileEntry) {
	                fileEntry.createWriter(function (writer) {
	                    console.log("Start creating image file");
	                    writer.seek(0);
	                    writer.write(dataurl);
	                    console.log("End creating image file. File created");
	                }, fail);
	            }, fail);
	        }, fail);
	    }, fail);
	    */
	}

	function fail(){
	    console.log("Error");
	}

	factory.uploadFirma = function(imagen) {
		var uri = encodeURI(SERVER_URL + "files/upload/1");

		var fd = new FormData();
		var imgBlob = new Blob([imagen], { type:'image/png'});
		console.log(imgBlob);
		fd.append('file', imgBlob, "firma");
		$http({
			  method: 'POST', data: fd,
				url: uri,
				headers: {'Content-Type': undefined}
			  //url: 'http://localhost:8080/fiscalizaciones_server/rest/login',
			  }).then(function (success) {
			  	console.log('************** '+JSON.stringify(success));
			//alert(JSON.stringify(success));
			}, function (error) {
				console.log('************** '+JSON.stringify(error));

				//alert(JSON.stringify(error));
			});
	}
  $ionicPlatform.ready(function() {
    //Crear los directorios necesarios
    if (window.cordova) {
      directorioAPP = cordova.file.dataDirectory;
      factory.crearDirectorioFotos();
    }
	});
	return factory;
}).service('MapsService', function($q, $localStorage, $cordovaGeolocation, $cordovaDialogs, toastr, ConfigService){
	var service = {};
	service.scope = {};
	service.resizeMap = function(){
		if(service.scope.map){
			var center = service.scope.map.getCenter();
			google.maps.event.trigger(service.scope.map, 'resize');
			service.scope.map.setCenter(center);
		}
	}
	var watch = null;
		var marker = null;
		var circle = null;

		var timeout = null;
		var tiempoEspera = 30000;


	var inicializarVariables = function(){
		service.scope.lastPosition = null;
		service.scope.puedeObtener = true;
	}
	service.createMapFromLatLong = function(){
		var isOnline = ConfigService.verificarConexion(createMapFromLatLongInternet, 'Verificar conexión a internet y/o activar GPS');
		if(!isOnline){
        	service.scope.ubicacion_info = "(lat: "+service.scope.fiscalizacion.latitud+", long: " +service.scope.fiscalizacion.longitud+")";
		}
	}

	var createMapFromLatLongInternet = function(){
		inicializarVariables(service.scope);
		var latLng = new google.maps.LatLng(service.scope.fiscalizacion.latitud, service.scope.fiscalizacion.longitud);
		var mapOptions = {
			  center: latLng,
			  zoom: 15,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
		service.scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
		var iconCircle ={
				    path: google.maps.SymbolPath.CIRCLE,
				    fillColor: 'gray',
				    fillOpacity: .9,
				    scale: 10,
				    strokeColor: 'white',
				    strokeWeight: 2
				};

		var marker = new google.maps.Marker({
			       clickable : false,
			       icon : iconCircle,
			       shadow : null,
			       zIndex : 999,
			       map : service.scope.map
			    });
		service.scope.map.setCenter(latLng);
		marker.setPosition(latLng);

	}
	service.createMap = function(){
		//$scope.map = undefined;
		if(service.scope.textoObtenerUbicacion != "Actualizando..."){
			service.scope.textoObtenerUbicacion = "Actualizando...";
		service.scope.botonObtenerUbicacion = "ion-ios-reload";

		var options = {timeout: 90000, enableHighAccuracy: true};

		$cordovaGeolocation.getCurrentPosition(options).then(function(position){

			if(!service.scope.lastPosition || position.coords.accuracy < service.scope.lastPosition.coords.accuracy){
				service.scope.lastPosition = position;
				service.scope.ubicacion_info = "Presición: " + position.coords.accuracy+" metros.";
			}

			var latLng = new google.maps.LatLng(service.scope.lastPosition.coords.latitude, service.scope.lastPosition.coords.longitude);
			var mapOptions = {
			  center: latLng,
			  zoom: 15,
			  mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			if(!service.scope.map){
				service.scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
			}else{
				if(marker){
					marker.setMap(null);
				}
			}

			/*Agregar un marcador en la posicion*/
			//Wait until the map is loaded

			google.maps.event.addListenerOnce(service.scope.map, 'idle', function(){


				var center = service.scope.map.getCenter();
				google.maps.event.trigger(service.scope.map, 'resize');
				service.scope.map.setCenter(center);
			});

			//Icono
			var iconCircle ={
			    path: google.maps.SymbolPath.CIRCLE,
			    fillColor: '#2468C5',
			    fillOpacity: 1,
			    scale: 7,
			    strokeColor: 'white',
			    strokeWeight: 2
			};


		    // Initialize drawing objects that will be used when we get a position.
		    marker = new google.maps.Marker({
		       clickable : false,
		       icon : iconCircle,
		       shadow : null,
		       zIndex : 999,
		       map : service.scope.map
		    });

		    circle = new google.maps.Circle({
		       fillColor : '#2468C5',
		       fillOpacity : 0.10,
		       strokeColor : 'blue',
		       strokeOpacity : 0.25,
		       strokeWeight : 0.5,
		       map : service.scope.map
		    });

		    service.scope.map.setCenter(latLng);
			marker.setPosition(latLng);
			circle.setCenter(latLng);
			circle.setRadius(service.scope.lastPosition.coords.accuracy);

			service.scope.map.fitBounds(circle.getBounds());
			var zoom = service.scope.map.getZoom();
			service.scope.map.setZoom(zoom > 16 ? 16 : zoom);

		    // Start the geolocation.
		    watch = navigator.geolocation.watchPosition(
		          positionSuccess,
		          positionError, {
		             enableHighAccuracy : true
		          });
		    // Set a timeout in case the geolocation accuracy never meets the threshold.
		    timeout = setTimeout(timeoutHandler, tiempoEspera);


			}, function(error){
				//POSITION_UNAVAILABLE
				//TIMEOUT
      console.log(angular.toJson(error.code, true));

      if(error.code == error.PERMISSION_DENIED){
					$cordovaDialogs.alert('Debe dar permiso a la APP para utilizar la ubicación.' +
						'Asigne el permiso desde las configuraciones del dispositivo','Error de permiso');
				}else if(error.code == error.TIMEOUT){
					$cordovaDialogs.alert('Verificar conexión a internet y/o activar GPS','Error de temporizador');
				}else{
					$cordovaDialogs.alert('Verificar conexión a internet y/o activar GPS','Error de conexión');
				}
				service.scope.textoObtenerUbicacion = "Obtener";
				service.scope.botonObtenerUbicacion = "ion-ios-location";
				service.scope.map = undefined;
								console.log(angular.toJson(error, true));

			}
		);
		}
	};
	//Cada vez que encuentra una posicion intenta actualizar
	var positionSuccess = function(position) {
		console.log("In _positionSuccess()");
		if(!service.scope.lastPosition || position.coords.accuracy < service.scope.lastPosition.coords.accuracy){
			service.scope.lastPosition = position;
			service.scope.ubicacion_info = "Presición: " + position.coords.accuracy+" metros.";

			var center = new google.maps.LatLng(
			      position.coords.latitude,
			      position.coords.longitude);
			service.scope.map.setCenter(center);
			marker.setPosition(center);
			circle.setCenter(center);
			circle.setRadius(position.coords.accuracy);
			service.scope.map.fitBounds(circle.getBounds());
			var zoom = service.scope.map.getZoom();
			service.scope.map.setZoom(zoom > 16 ? 16 : zoom);
			if (position.coords.accuracy < 50) {
			   // Accept any position with an accuracy of < 50m.
			   acceptPosition();
			}
			if(!service.scope.$$phase) {
			  service.scope.$apply();
			}
		}
 	}

 	//Error al obtener posicion
 	var positionError = function(error) {
		console.log("In _positionError()");
		clearTimeout(timeout);
        $cordovaGeolocation.clearWatch(watch);
        service.scope.ubicacion_info = "(lat: "+service.scope.lastPosition.coords.latitud+", long: " +service.scope.fiscalizacion.coord.longitud+")";
        $cordovaDialogs.alert('Verificar conexión a internet y/o activar GPS','Error de conexión');
        service.scope.textoObtenerUbicacion = "Actualizar";
		service.scope.botonObtenerUbicacion = "ion-ios-reload";
		if(!service.scope.$$phase) {
		  service.scope.$apply();
		}
 	}

 	service.clearWatch = function(){
 		if (watch != null) {
	       $cordovaGeolocation.clearWatch(watch);
	    }
	    watch = null;
	    if (timeout != null) {
	       clearTimeout(timeout);
	    }
	    timeout = null;
 	}

 	//Una vez que la posicion es aceptada por alcanzar la precision requerida
 	// o por agotarse el tiempo de espera
 	var acceptPosition = function() {
	    console.log("In _acceptPosition()");
	    // Shut off geolocation and timeout.
	    if (watch != null) {
	       $cordovaGeolocation.clearWatch(watch);
	    }
	    watch = null;
	    if (timeout != null) {
	       clearTimeout(timeout);
	    }
	    timeout = null;
	    // Remove the accuracy circle from the map
	    circle.setMap(null);
	    service.scope.textoObtenerUbicacion = "Actualizar";
		service.scope.botonObtenerUbicacion = "ion-ios-reload";
		if(!service.scope.$$phase) {
		  service.scope.$apply();
		}
	 }

	//Si se acaba el tiempo, por no encontrar una distancia aceptable o
	// por finalizar el temporizador
	var timeoutHandler = function(error) {
		console.log(error);
	    console.log("In _timeoutHandler()");
	    var keepWaitingHandler = function() {
	       timeout = setTimeout(timeoutHandler, tiempoEspera);
	    };

	    if(window.cordova){
			$cordovaDialogs.confirm('No se pudo obtener una mejor ubicación','Ubicación',
				['Usar','Esperar'])
				.then(function(buttonIndex) {
				// no button = 0, 'OK' = 1, 'Cancel' = 2
				var btnIndex = buttonIndex;
				if(btnIndex == 1){
					acceptPosition();
				}else{
					keepWaitingHandler();
				}
			});
	    }else{//Solo para pruebas
	    	var ok = confirm('No se pudo obtener una mejor ubicación');
	    	if(ok){
				acceptPosition();
			}else{
				keepWaitingHandler();
			}
	    }
	}
	return service;
}).service('FirmaService', function($q, $localStorage){
	var service = {};
	var canvas;
	var signaturePad;
	var tipoFirma;
	service.scope = {};

	service.cerrarFirmaModal = function() {
    screen.orientation.lock('portrait');
    service.scope.firmaModal.hide();
	};

	var inicializarCanvas = function(canvas) {//Para que el Canvas sea
		var
		// Obtain a reference to the canvas element
		// using its id.
		htmlCanvas = canvas,

	  	// Obtain a graphics context on the
	  	// canvas element for drawing.
	  	context = htmlCanvas.getContext('2d');

		// Start listening to resize events and
		// draw canvas.
		initialize();

		function initialize() {
			// Register an event listener to
			// call the resizeCanvas() function each time
			// the window is resized.
			window.addEventListener('resize', resizeCanvas, false);

			// Draw canvas border for the first time.
			resizeCanvas();
		}
		// Runs each time the DOM window resize event fires.
		// Resets the canvas dimensions to match window,
		// then draws the new borders accordingly.
		function resizeCanvas() {
			htmlCanvas.width = window.innerWidth;
			htmlCanvas.height = window.innerHeight;
		}
	}

	service.mostrarFirmaModal = function(firma){
		tipoFirma = firma;
		service.scope.firmaModal.show();
		canvas = document.getElementById('signatureCanvas');
		signaturePad = new SignaturePad(canvas, {
	    minWidth: 1,
	    maxWidth: 1.2
		});
		inicializarCanvas(canvas);
	};

	service.clearCanvas = function() {
		if(!signaturePad.isEmpty()){
        	signaturePad.clear();
        }else{
        	signaturePad.clear();
			service.scope.saveCanvas();
        }

    };

    service.saveCanvas = function() {
        var myImage = new Image();
		myImage.crossOrigin = "Anonymous";
		myImage.onload = function(){
			if(!signaturePad.isEmpty()){
	        	var imageData = removeImageBlanks(myImage); //Will return cropped image data

			    if(tipoFirma == 1){
	         		localStorage.setItem("firma",imageData);
	         	}else{
	         		localStorage.setItem("firmaContratista",imageData);
	         	}
	        }else{
	        	if(tipoFirma == 1){
	         		localStorage.removeItem('firma');
	         	}else{
	         		localStorage.removeItem('firmaContratista');
	         	}
	        }
	         service.scope.$apply(function () {
	         	if(tipoFirma == 1){
	         		service.scope.firma = imageData;
	         	}else{
	         		service.scope.firmaContratista = imageData;
	         	}

			});
		}
		myImage.src = signaturePad.toDataURL();
        service.cerrarFirmaModal();
    };

    function removeImageBlanks(imageObject) {
	    var imgWidth = imageObject.width;
	    var imgHeight = imageObject.height;

	    var canvas = document.createElement('canvas');
	    canvas.setAttribute("width", imgWidth);
	    canvas.setAttribute("height", imgHeight);
	    var context = canvas.getContext('2d');
	    context.fillStyle = "rgb(255,255,255)"
		context.fillRect(0, 0, imgWidth, imgHeight);
	    context.drawImage(imageObject, 0, 0);

	    var imageData = context.getImageData(0, 0, imgWidth, imgHeight),
	        data = imageData.data,
	        getRBG = function(x, y) {
	            var offset = imgWidth * y + x;
	            return {
	                red:     data[offset * 4],
	                green:   data[offset * 4 + 1],
	                blue:    data[offset * 4 + 2],
	                opacity: data[offset * 4 + 3]
	            };
	        },
	        isWhite = function (rgb) {
	            // many images contain noise, as the white is not a pure #fff white
	            return rgb.red > 200 && rgb.green > 200 && rgb.blue > 200;
	        },
	        scanY = function (fromTop) {
	            var offset = fromTop ? 1 : -1;
	            // loop through each row
	            for(var y = fromTop ? 0 : imgHeight - 1; fromTop ? (y < imgHeight) : (y > -1); y += offset) {
	                // loop through each column
	                for(var x = 0; x < imgWidth; x++) {
	                    var rgb = getRBG(x, y);
	                    if (!isWhite(rgb)) {
	                        return y;
	                    }
	                }
	            }
	            return null; // all image is white
	        },
	        scanX = function (fromLeft) {
	            var offset = fromLeft? 1 : -1;
	            // loop through each column
	            for(var x = fromLeft ? 0 : imgWidth - 1; fromLeft ? (x < imgWidth) : (x > -1); x += offset) {
	                // loop through each row
	                for(var y = 0; y < imgHeight; y++) {
	                    var rgb = getRBG(x, y);
	                    if (!isWhite(rgb)) {
	                        return x;
	                    }
	                }
	            }
	            return null; // all image is white
	        };

	    var cropTop = scanY(true),
	        cropBottom = scanY(false),
	        cropLeft = scanX(true) -10,
	        cropRight = scanX(false) - 10,
	        cropWidth = cropRight - cropLeft + 10,
	        cropHeight = cropBottom - cropTop + 10;
	    canvas.setAttribute("width", cropWidth);
	    canvas.setAttribute("height", cropHeight);
	    // finally crop the guy
	    canvas.getContext("2d").drawImage(imageObject,
	        cropLeft, cropTop, cropWidth, cropHeight,
	        0, 0, cropWidth, cropHeight);

	    return canvas.toDataURL();
	}
	return service;
})
;
