angular.module('app.controllers', [])

.controller('loginCtrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $ionicPopup, $state, $cordovaNetwork, $timeout, $ionicPlatform,
	$localStorage, DatosFactory, $cordovaDialogs, $rootScope, $ionicHistory) {
	$scope.logging = false;

	if($localStorage.sesionIniciada){
    if($localStorage.fiscalizacionBackup){
      if(JSON.stringify($localStorage.fiscalizacionBackup) == "{}"){
        $state.go('menu.locales');
      }
    }else {
      $state.go('menu.locales');
    }

	}

	$scope.$on('$ionicView.enter', function() {
		$ionicHistory.clearHistory();
	});


	$scope.data = {};
	$scope.error = 'hides';
	$scope.mensajeError = '';
	$scope.data.recordar = $localStorage.recordar;
	if($localStorage.recordar == undefined){
		$scope.data.recordar = true;
	}
	$scope.data.username = $localStorage.username;
	$scope.data.password = $localStorage.password;

	var dialogoErrorConexion = function(mensajeError, tituloError){
		var mensaje = mensajeError || 'Error de conexión';
		var titulo = tituloError || 'Error';
		if(window.cordova){
			$cordovaDialogs.alert(mensaje, titulo);
	    }else{//Solo para pruebas
	    	alert(mensaje);
	    }
	};

	var dialogoTrabajarSinConexion = function(mensajeError, tituloError){
		var mensaje = mensajeError || '¿Desea trabajar sin conexión?';
		var titulo = tituloError || 'Error de conexión';
		if(window.cordova){
			$cordovaDialogs.confirm(mensaje,titulo,
				['OK','Cancelar'])
				.then(function(buttonIndex) {
				// no button = 0, 'OK' = 1, 'Cancel' = 2
				var btnIndex = buttonIndex;
				if(btnIndex == 1){
		 			$state.go('menu.locales');
				}
			});
	    }else{//Solo para pruebas
	    	var ok = confirm('Error de conexión al servidor\n\n¿Desea trabajar sin conexión?');
	    	if(ok){
		 		$state.go('menu.locales');
			}
	    }
	};
	var sesionIniciada = function(fiscalizador){
		$localStorage.fiscalizador = fiscalizador;
		$rootScope.fiscalizador = $localStorage.fiscalizador;
		$localStorage.usernameActivo = $scope.data.username;
		$localStorage.passwordActivo = $scope.data.password;
		$localStorage.sesionIniciada = true;
		$state.go('menu.locales');
	};

	var iniciarLogin = function(){
		$scope.logging = true;
		DatosFactory.login($scope.data.username, $scope.data.password).
    	success(function(success) {
    		$scope.logging = false;
    		console.log(angular.toJson(success, true));
    		var fiscalizador = success;
    		if($localStorage.locales){
    			if($localStorage.usernameActivo != $scope.data.username){
    				if(window.cordova){
              $cordovaDialogs.confirm('¿Desea sobreescribir los datos del usuario actual?','Datos existentes',
                ['OK','Cancelar'])
                .then(function(buttonIndex) {
                // no button = 0, 'OK' = 1, 'Cancel' = 2
                var btnIndex = buttonIndex;
                if(btnIndex == 1){
                  delete $localStorage.locales;
                  $localStorage.fiscalizacionBackup = {};
                  $localStorage.recuperando = false;
                  sesionIniciada(fiscalizador);
                }
              });
				    }else{//Solo para pruebas
				    	var ok = confirm('¿Desea sobreescribir los datos del usuario actual?');
				    	if(ok){
                delete $localStorage.locales;
                  $localStorage.fiscalizacionBackup = {};
                  $localStorage.recuperando = false;
                  sesionIniciada(fiscalizador);
              }
				    }
    			}else{
					sesionIniciada(fiscalizador);
    			}
    		}else{
    			sesionIniciada(fiscalizador);
    		}

   		}).error(function(data, status) {
   			$scope.logging = false;
      console.log(angular.toJson(data, true));
      console.log(angular.toJson(status, true));
   			if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard){
   				cordova.plugins.Keyboard.close();
   			}

   			if(data){
   				//Solo se le permitirá trabajar sin conexión si ya tuvo acceso antes
	        	dialogoErrorConexion(data.detail, data.error);
   			}else{
   				if($localStorage.usernameActivo && $localStorage.passwordActivo){
	        		if($localStorage.usernameActivo == $scope.data.username && $localStorage.passwordActivo == $scope.data.password){
						dialogoTrabajarSinConexion();
	        		}else{
	        			dialogoErrorConexion();
	        		}
	        	}else{
	        		dialogoErrorConexion();
	        	}
   			}
		});
	}

    $scope.login = function() {
    	$localStorage.recordar = $scope.data.recordar;
    	if($localStorage.recordar){
    		$localStorage.username = $scope.data.username;
    		$localStorage.password = $scope.data.password;
    	}else{
    		delete $localStorage.username;
    		delete $localStorage.password;
    	}
		var isOnline = true;
    	if(window.cordova){
    		isOnline = $cordovaNetwork.isOnline();
    	}

    	if(isOnline){
    		iniciarLogin();
    	}else{
    		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard){
   				cordova.plugins.Keyboard.close();
   			}

   			//Solo se le permitirá trabajar sin conexión si ya tuvo acceso antes
        	if($localStorage.usernameActivo && $localStorage.passwordActivo
        		&& $localStorage.usernameActivo == $scope.data.username
        		&& $localStorage.passwordActivo == $scope.data.password){
					dialogoTrabajarSinConexion();
        	}else{
        		if(window.cordova){
					$cordovaDialogs.alert('Verificar conexión a internet.\nSolo el último usuario conectado puede acceder sin conexión','Error de conexión');
			    }else{//Solo para pruebas
			    	alert('Error de conexión\n\nVerificar conexión a internet.\nSolo el último usuario conectado puede acceder sin conexión');
			    }
        	}
    	}
    }

    $scope.probarApp = function(){
    	//PARA PODER TRABAJAR LOCALMENTE CON UN ARCHIVO DE PRUEBA
        DatosFactory.getDatosLocales().success(function(data) {
          console.log($localStorage.locales);
			if(data.status == "0"){
				$localStorage.locales = data.objeto.locales;
        $localStorage.situaciones = data.objeto.situaciones;
        $localStorage.tipos_verificaciones = data.objeto.tipos_verificaciones;

				$localStorage.sesionIniciada = false;
				$state.go('menu.locales');
			}
   		})

    };
})

.controller('localesCtrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $localStorage, $state, $ionicHistory, $cordovaDialogs, $ionicListDelegate,
          DatosFactory, $timeout, $cordovaNetwork, $ionicPlatform, $ionicSideMenuDelegate) {
	$scope.titulo = "Locales";
  var actualizarSinPendientes = function () {
    var isOnline = true;
    if(window.cordova){
      isOnline = $cordovaNetwork.isOnline();
    }
    if(isOnline){
      if($localStorage.sesionIniciada && !DatosFactory.tienePendientes()){//Si aún no hay pendientes se actualiza
        $timeout(function() {
          obtenerDatos();
        });
      }//Si ya tiene datos se espera descarga manual desde el menu
    }
  }
	$ionicPlatform.ready(function() {
    if($localStorage.fiscalizacionBackup && JSON.stringify($localStorage.fiscalizacionBackup) != "{}"){
      $timeout(function() {
        dialogoRecuperacion();
      }, 300);
    }else{
      actualizarSinPendientes();
    }
	});



  var dialogoRecuperacion = function () {
    if(window.cordova){
      $cordovaDialogs.confirm('Se guardó un respaldo tras el cierre inesperado de la aplicación. \n¿Desea recuperar los datos?','Recuperación de Fiscalizacion',
        ['OK','Cancelar'])
        .then(function(buttonIndex) {
          // no button = 0, 'OK' = 1, 'Cancel' = 2
          var btnIndex = buttonIndex;
          if(btnIndex == 1){
            $localStorage.recuperando = true;
            $ionicHistory.nextViewOptions({
              disableBack: true
            });
            $state.go('menu.fiscalizacion');
          }else{
            $localStorage.fiscalizacionBackup = {};
            $localStorage.recuperando = false;
            actualizarSinPendientes();
          }
        });
    }else{//Solo para pruebas
      var ok = confirm('Se guardó un respaldo tras el cierre inesperado de la aplicación. \n¿Desea recuperar los datos?');
      if(ok){
        $localStorage.recuperando = true;
        $ionicHistory.nextViewOptions({
          disableBack: true
        });
        $state.go('menu.fiscalizacion');
      }else{
        $localStorage.recuperando = false;
        $localStorage.fiscalizacionBackup = {};
        actualizarSinPendientes();
      }
    }
  }

	$scope.masInfo = function(local){
		if(window.cordova){
		 $cordovaDialogs.alert(local.establecimiento.descripcion, 'Establecimiento', 'OK')
		    .then(function() {
		      $ionicListDelegate.closeOptionButtons();
		    });
		}else{
			alert(local.establecimiento.descripcion);
			$ionicListDelegate.closeOptionButtons();
		}
	};

	$scope.contarPendientes = function(local){
		return DatosFactory.contarPendientesLocal(local);
	}

	$scope.$on('$ionicView.enter', function() {
    $ionicSideMenuDelegate.canDragContent(true);
		$ionicHistory.clearHistory();
		$scope.locales = $localStorage.locales;
    console.log($localStorage.locales);

  });

	$scope.$on('locales:updated', function(event,data) {
            // you could inspect the data to see if what you care about changed, or just update your own scope
            $scope.locales = $localStorage.locales;
        });
  $scope.$on('fiscalizacionesTotal:updated', function(event,data) {
    $ionicHistory.clearHistory();
    $scope.locales = $localStorage.locales;
  });

	var obtenerDatos = function(){
		DatosFactory.setScope($scope);
		DatosFactory.getDatos(true);
	}

	var dialogoErrorConexion = function(){
		if(window.cordova){
			$cordovaDialogs.alert('Error de conexión con el servidor', 'Error');
	    }else{//Solo para pruebas
	    	alert('Error de conexión');
	    }
	};

	$scope.ir = function(local){
		$ionicListDelegate.closeOptionButtons();
		$localStorage.localGlobal = local;
		$state.go('menu.obras');
	}
})

.controller('obrasCtrl',  // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $ionicListDelegate, $state, $localStorage, $cordovaDialogs, DatosFactory) {
	$scope.titulo = "Obras";
	$scope.local = $localStorage.localGlobal;
	$scope.obras = $scope.local.obras;

	$scope.$on('$ionicView.enter', function() {
		if($localStorage.localGlobal == undefined){
			$scope.local = $localStorage.localGlobal;
			$scope.obras = $scope.local.obras;
		}
	});

	$scope.contarPendientes = function(obra){
		return DatosFactory.contarPendientesObra(obra);
	}

	$scope.ir = function(obra){
		$ionicListDelegate.closeOptionButtons();
		$localStorage.obraGlobal = obra;
		$state.go('menu.fiscalizaciones');
	}
	$scope.masInfo = function(obra){
		if(window.cordova){
		 $cordovaDialogs.alert(obra.obra.descripcion, 'Obra', 'OK')
		    .then(function() {
		      $ionicListDelegate.closeOptionButtons();
		    });
		}else{
			alert(obra.obra.descripcion);
			$ionicListDelegate.closeOptionButtons();
		}

	}
})

.controller('fiscalizacionesCtrl',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $localStorage, $state, FotosService, DatosFactory,
	$cordovaSpinnerDialog, $timeout, $cordovaDialogs, $ionicListDelegate,
	PDFFactory, $cordovaActionSheet) {
	$scope.titulo = "Fiscalizaciones";
	$scope.date = (new Date());
	$scope.obra = $localStorage.obraGlobal;
	$scope.fiscalizaciones = $scope.obra.fiscalizaciones;
	$localStorage.prueba = $localStorage.localGlobal;
	var prueba = $localStorage.prueba;
	var obras = prueba.obras;
  //obras[0].obra = "aaa";
  //obras[0].fiscalizaciones[0].longitud = "aaa";
	//prueba.establecimiento = "aaa";
	$scope.$on('$ionicView.enter', function() {
		if($localStorage.obraGlobal == undefined){
			$scope.obra = $localStorage.obraGlobal;
		}
		$scope.fiscalizaciones = $scope.obra.fiscalizaciones;
	});

	//Cuando se reciben todas las ficalizaciones actualizadas y guardadas
  $scope.$on('fiscalizaciones:updated', function(event,data) {
    // you could inspect the data to see if what you care about changed, or just update your own scope
    $scope.obra = $localStorage.obraGlobal;
    $scope.fiscalizaciones = $scope.obra.fiscalizaciones;
  });

	//se obtiene el mayor id_avance falso (antes de ser guardado) para ir asignando
	//a las nuevas fiscalizaciones
	/*
	var id_avance_mayor = 0;
	for(i in $scope.fiscalizaciones){
		var id_avance  = $scope.fiscalizaciones[i].id_avance;
		if(id_avance < 0){
			if(id_avance_mayor > id_avance){
				id_avance_mayor = id_avance;
			}
		}
	}
	*/
	//$localStorage.id_avance = id_avance_mayor;

	var verificarCertificados = function(){
		var encontro = false;
		if($scope.fiscalizaciones){
			for(i = $scope.fiscalizaciones.length -1; i >= 0; i--){
				if($scope.fiscalizaciones[i].numero_certificado == undefined){
					$scope.fiscalizaciones[i].numero_certificado = 0;
				}
				if($scope.fiscalizaciones[i].numero_certificado > 0
					&& !encontro){
					encontro = true;
				}
				if($scope.fiscalizaciones[i].numero_certificado == 0
					&& encontro){
					$scope.fiscalizaciones[i].numero_certificado = -1;
				}
			}
		}
	};
	verificarCertificados();
  $scope.$on('certificado:updated', function(event,data) {
    verificarCertificados();
  });
	$scope.verificarPendientes = function(){
		for(i in $scope.fiscalizaciones){
			if($scope.fiscalizaciones[i].estado){
				return true;
			}
		}
		return false;
	};

	$scope.mostrarActionSheet = function(fiscalizacion){
		var opciones = {
		    ACTA_PDF : 'Acta de Medición (PDF)',
		    INFORME_PDF : 'Informe de Fiscalización (PDF)',
		    CERTIFICADO_PDF : 'Certificado de obras  (PDF)',
		    CERTIFICADO_GENERAR : 'Generar Certificado de Obras',
		    INFORME_GENERAR : 'Cargar Informe de Fiscalización',
		    CANCELAR : 'Cancelar',
		    ELIMINAR : 'Eliminar'
		};
		var botones = [];
		if(fiscalizacion.id_avance > 0){
			botones.push(opciones.ACTA_PDF);
			botones.push(opciones.INFORME_PDF);
			if(fiscalizacion.numero_certificado > 0){
				botones.push(opciones.CERTIFICADO_PDF);
			}
			if(fiscalizacion.numero_certificado == 0 && fiscalizacion.estado != 'actualizando'){
				botones.push(opciones.CERTIFICADO_GENERAR);
			}
			//botones.push(opciones.INFORME_GENERAR);
		}
		var eliminar;
		if(fiscalizacion == $scope.fiscalizaciones[$scope.fiscalizaciones.length - 1] && fiscalizacion.estado == 'guardado'){
			eliminar = opciones.ELIMINAR;
		}

		var options = {
		        androidTheme: window.plugins.actionsheet.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT, // default is THEME_TRADITIONAL
		        title: 'Seleccione una acción',
		        //subtitle: 'Choose wisely, my friend', // supported on iOS only
		        buttonLabels: botones,
		        androidEnableCancelButton : true, // default false
		        addCancelButtonWithLabel: opciones.CANCELAR,
		        addDestructiveButtonWithLabel : eliminar,
		        position: [20, 40], // for iPad pass in the [x, y] position of the popover
		        destructiveButtonLast: true // you can choose where the destructive button is shown
		    };
		    // Depending on the buttonIndex, you can now call shareViaFacebook or shareViaTwitter
		    // of the SocialSharing plugin (https://github.com/EddyVerbruggen/SocialSharing-PhoneGap-Plugin)
			$cordovaActionSheet.show(options)
		      .then(function(btnIndex) {
		        var index = btnIndex;
		        index = index - 1;
		        var opcion = botones[index];

	        	switch (opcion)
		        {
		            case opciones.ACTA_PDF:
		            	$scope.obtenerPdf(fiscalizacion, 2);
		            	break;
		            case opciones.INFORME_PDF:
		            	$scope.obtenerPdf(fiscalizacion, 3);
		                break;
		            case opciones.CERTIFICADO_PDF:
		            	$scope.obtenerPdf(fiscalizacion, 1);
		                break;
		            case opciones.CERTIFICADO_GENERAR:
		            	$scope.generarCertificado(fiscalizacion);
		                break;
		            case opciones.INFORME_GENERAR:
		            	alert('falta implementar');
		                break;
		            case opciones.ELIMINAR:
		            	$scope.eliminarUltimo();
		                break;
		            default:
				        if(index == 0){//eliminar
				        	$scope.eliminarUltimo();
			        	}
		                break;
		        }


		      });

	};

	$scope.ir = function(fiscalizacion){
		$localStorage.fiscalizacionGlobal = fiscalizacion;
    $ionicListDelegate.closeOptionButtons();
		$state.go('menu.fiscalizacion');
	};
	$scope.info = function(){
		alert('pantalla para generar certificado de obras?');
	};
	$scope.eliminarUltimo = function(){
		if(window.cordova){
			$cordovaDialogs.confirm('¿Está seguro que quiere eliminar esta fiscalización?','Eliminar',
				['Aceptar','Cancelar'])
				.then(function(buttonIndex) {
				// no button = 0, 'OK' = 1, 'Cancel' = 2
				var btnIndex = buttonIndex;
				if(btnIndex == 1){
					var eliminado = $scope.fiscalizaciones.pop();
					FotosService.borrarDirectorioFiscalizacion(eliminado);
				}
			});
	    }else{//Solo para pruebas
	    	var ok = confirm('¿Está seguro que quiere eliminar esta fiscalización?');
	    	if(ok){
				$scope.fiscalizaciones.pop();
				//$localStorage.id_avance = $localStorage.id_avance + 1;
			}
	    }
	};

	$scope.enviarPendientes = function(){
    var pendientes = [];
    $scope.fiscalizaciones.forEach(function(fiscalizacion){
      if(fiscalizacion.estado){
        pendientes.push(fiscalizacion);
      }
    });
    if(pendientes.isEmpty){
      if(window.cordova){
        $cordovaDialogs.alert('No tiene fiscalizaciones pendientes','Sin pendientes');
      }else{//Solo para pruebas
        alert('No tiene fiscalizaciones pendientes');
      }
    }else{
        DatosFactory.enviarPendientes(pendientes, true);
    }

  }

	var ocultarDiaogo = function(){
            $cordovaSpinnerDialog.hide();
            //$ionicListDelegate.closeOptionButtons();
	};


	$scope.generarCertificado = function(fiscalizacion){
        if(window.cordova){
                $cordovaDialogs.confirm('¿Está seguro que quiere generar el Certificado de Obras?','Generar',
                ['Aceptar','Cancelar'])
            .then(function(buttonIndex) {
              // no button = 0, 'OK' = 1, 'Cancel' = 2
              if(buttonIndex == 1){
                console.log('generando certificado');
                DatosFactory.generarCertificado(fiscalizacion);
                verificarCertificados();
              }
            });
        }else{//Solo para pruebas
            var ok = confirm('Está seguro que quiere generar el Certificado de Obras?');
            if(ok){
                var mayor = 0;
                for(i in $scope.fiscalizaciones){
                        if($scope.fiscalizaciones[i].numero_certificado > mayor){
                                mayor = $scope.fiscalizaciones[i].numero_certificado;
                        }
                }
                fiscalizacion.numero_certificado = mayor +1;
                verificarCertificados();
            }
        }
	};

	$scope.obtenerPdf = function(fiscalizacion, tipo){
            var url ="";
            if(tipo == 1){
                    url = "http://181.122.120.58:8080/fiscalizaciones_server/rest/informes/emitir_informe_servidor?"+
                    "formato=pdf&nombre_reporte=rptCertificadoObra.rptdesign&parametros=%26parametros0="+ fiscalizacion.id_avance;
            }else if(tipo == 2){
                    url = "http://181.122.120.58:8080/fiscalizaciones_server/rest/informes/emitir_informe_servidor?"+
                    "formato=pdf&nombre_reporte=rptActaMedicion.rptdesign&parametros=%26parametros0=" + fiscalizacion.id_avance;
            }else{
                    url = "http://181.122.120.58:8080/fiscalizaciones_server/rest/informes/emitir_informe_servidor?"+
                    "formato=pdf&nombre_reporte=rptInformeFiscalizacion.rptdesign&parametros=%26parametros0%3D"
                      +fiscalizacion.id_avance+"%26parametros1%3D" + +fiscalizacion.solicitud_intervencion_id;
            }
            console.log(url);
            PDFFactory.downloadPDF(url);
	};
})

.controller('menuCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $localStorage, toastr, PDFFactory, $cordovaDialogs, $rootScope, DatosFactory, $ionicModal, $cordovaNetwork) {
    $scope.$on('$ionicView.enter', function() {
		$scope.sesionIniciada = $localStorage.sesionIniciada;
		setTitulo();
	});

	$ionicModal.fromTemplateUrl('templates/politicas.html', {
    scope: $scope,
    animation: 'slide-in-up'
	  }).then(function(modal) {
	    $scope.modal = modal;
	  });

	$scope.politicas = function(){
		$scope.modal.show();
	}
	$scope.cararPoliticas = function(){
		$scope.modal.hide();
	}

	$scope.contarPendientes = function(){
		return DatosFactory.contarPendientes();
	}


    var setTitulo = function(){
    	if($localStorage.sesionIniciada)
	        $scope.iniciarSesionTitulo = 'Cerrar sesión';
		else
	        $scope.iniciarSesionTitulo = 'Iniciar sesión';
    }
    setTitulo();
	$rootScope.fiscalizador = $localStorage.fiscalizador;

	$scope.cerrarSesion = function(){
        $localStorage.sesionIniciada = false;
        $scope.sesionIniciada = $localStorage.sesionIniciada;

        setTitulo();
        $state.go('login');
	};
	$scope.salir = function(){
            console.log('saliendo');
            ionic.Platform.exitApp();
	};

	$scope.actualizar = function(){
		verificarConexion(actualizarDatos);
	}

	var verificarConexion = function(funcion){
		var isOnline = true;
    	if(window.cordova){
    		isOnline = $cordovaNetwork.isOnline();
    	}
    	if(isOnline){
    		funcion();
    	}else{
    		if(window.cordova){
				$cordovaDialogs.alert('Verificar conexión a internet','Error de conexión');
		    }else{//Solo para pruebas
		    	alert('Verificar conexión a internet');
		    }
    	}
	}

	/*Actualizar los datos locales obtenidos del servidor
	*/
	var actualizarDatos = function() {
		if($localStorage.sesionIniciada){
			if(DatosFactory.tienePendientes()){
				if(window.cordova){
	                $cordovaDialogs.confirm('Las fiscalizaciones pendientes se perderán.\n¿Está seguro que desea actualizar los datos?','Fiscalizaciones pendientes',
		                    ['Aceptar','Cancelar'])
		                    .then(function(buttonIndex) {
		                    // no button = 0, 'OK' = 1, 'Cancel' = 2
		                    var btnIndex = buttonIndex;
		                    if(btnIndex == 1){
	                            obtenerDatos();
		                    }
		                });
			    }else{//Solo para pruebas
			    	var ok = confirm('Las fiscalizaciones pendientes se perderán\n¿Está seguro que desea actualizar los datos?');
			    	if(ok){
	                    obtenerDatos();
	                }
			    }
			}else{
				obtenerDatos();
			}

		}else{
			if(window.cordova){
                $cordovaDialogs.confirm('¿Iniciar sesión?','Sesión requerida',
                    ['Aceptar','Cancelar'])
	                    .then(function(buttonIndex) {
	                    // no button = 0, 'OK' = 1, 'Cancel' = 2
	                    var btnIndex = buttonIndex;
	                    if(btnIndex == 1){
	                        $state.go('login');
	                    }
	                });
		    }else{//Solo para pruebas
		    	var ok = confirm('Iniciar sesion?');
		    	if(ok){
                    $state.go('login');
                }
		    }
		}
	};

	var obtenerDatos = function(){
		DatosFactory.getDatos();
	}

  $scope.sincronizar = function() {
    var pendientes = [];
    if($localStorage.locales)
    angular.forEach($localStorage.locales, function(local) {
      if(local.obras)
      angular.forEach(local.obras, function(obra) {
        if(obra.fiscalizaciones)
        angular.forEach(obra.fiscalizaciones, function(fiscalizacion) {
          if(fiscalizacion.estado) {
            pendientes.push(fiscalizacion);
          }
        });
      });
    });

    if(pendientes.isEmpty){
      if(window.cordova){
        $cordovaDialogs.alert('No tiene fiscalizaciones pendientes','Sin pendientes');
      }else{//Solo para pruebas
        alert('No tiene fiscalizaciones pendientes');
      }
    }else{
      DatosFactory.enviarPendientes(pendientes, false);
    }
	};

	$scope.acercaDe = function() {
            if(window.cordova){
                $cordovaDialogs.alert('Icon made by Freepik from www.flaticon.com','Versión 1.0.0', 'OK')
                        .then(function(buttonIndex) {
                });
	    }else{//Solo para pruebas
	    	var ok = alert('Icon made by Freepik from www.flaticon.com');
	    }
	};

	$scope.verManual = function() {
		//url = "http://users.dsic.upv.es/~rnavarro/NewWeb/docs/RestVsWebServices.pdf";
		url = "data/manual.pdf";
		PDFFactory.downloadPDF(url);
	};
})

.controller('fiscalizacionCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $state, $ionicModal, $cordovaCamera, $cordovaDialogs, $ionicHistory,
    $ionicSlideBoxDelegate, $localStorage, toastr, $timeout, $ionicScrollDelegate, $ionicSideMenuDelegate,
    FotosService, MapsService, FirmaService, $rootScope, $ionicPlatform, ionicDatePicker) {
    MapsService.scope = $scope;
    $scope.titulo = "Fiscalización";
    //$scope.idObra = $stateParams.idObra;
    $scope.go = $state.go.bind($state);
    $scope.tipos_verificaciones = angular.copy($localStorage.tipos_verificaciones);
    $scope.observaciones = [];

  $scope.obra = $localStorage.obraGlobal;

  $scope.agregarObservacion = function () {
      if($scope.fiscalizacion.observaciones == undefined){
        $scope.fiscalizacion.observaciones = [];
      }
      $scope.fiscalizacion.observaciones.push({});
      $ionicScrollDelegate.scrollBottom();
   }

  $scope.borrarObservacion = function (observacion) {
    if(observacion.id){
      observacion.id = - observacion.id;
    }else{
      var i = $scope.fiscalizacion.observaciones.indexOf(observacion);
      $scope.fiscalizacion.observaciones.splice(i, 1);
    }
  }

   $scope.descartar = function () {
     //$localStorage.recuperando = false;
     doCustomBack();
   }
    //Configuracion para evitar que salga del formulario sin antes guardar
    // run this function when either hard or soft back button is pressed
    var doCustomBack = function() {
      console.log('modo consulta: ' +$scope.modoConsulta());

      if( !$scope.modoConsulta()){
        if(window.cordova){
          $cordovaDialogs.confirm('Los datos no guardados se perderán','¿Está seguro?',
            ['Aceptar','Cancelar'])
            .then(function(buttonIndex) {
            // no button = 0, 'OK' = 1, 'Cancel' = 2
            var btnIndex = buttonIndex;
            if(btnIndex == 1){
              $localStorage.fiscalizacionBackup = {};
              localStorage.setItem('firma', "");
              localStorage.setItem('firmaContratista', "");
              if($localStorage.recuperando){
                $ionicHistory.nextViewOptions({
                  disableBack: true
                });
                deregister();
                $localStorage.recuperando = false;
                MapsService.clearWatch();
                FotosService.vaciarDirectorioTemp();
                $state.go("menu.locales");
              }else{
                oldSoftBack();
              }

            }
          });
        }else{//Solo para pruebas
            var ok = confirm('¿Está seguro?\nLos datos no guardados se perderán.');
            if(ok){
              $localStorage.fiscalizacionBackup = {};
              localStorage.setItem('firma', "");
              localStorage.setItem('firmaContratista', "");
              console.log('recusss: '+$localStorage.recuperando);

              if($localStorage.recuperando){
                $ionicHistory.nextViewOptions({
                  disableBack: true
                });
                deregister();
                $localStorage.recuperando = false;
                console.log('recu: '+$localStorage.recuperando);
                MapsService.clearWatch();
                FotosService.vaciarDirectorioTemp();
                $state.go("menu.locales");
              }else{
                oldSoftBack();
              }
                }
        }
      }else{
        $localStorage.fiscalizacionBackup = {};
        localStorage.setItem('firma', "");
        localStorage.setItem('firmaContratista', "");
        if($localStorage.recuperando){
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          deregister();
          $localStorage.recuperando = false;
          console.log('recu: '+$localStorage.recuperando);
          MapsService.clearWatch();
          FotosService.vaciarDirectorioTemp();
          $state.go("menu.locales");

        }else{
          oldSoftBack();

        }
      }
    };

    // override soft back
    // framework calls $rootScope.$ionicGoBack when soft back button is pressed
    var oldSoftBack = $rootScope.$ionicGoBack;
    $rootScope.$ionicGoBack = function() {
        doCustomBack();
    };
    var deregisterSoftBack = function() {
        $rootScope.$ionicGoBack = oldSoftBack;
    };

    // override hard back
    // registerBackButtonAction() returns a function which can be used to deregister it
    var deregisterHardBack = $ionicPlatform.registerBackButtonAction(
        doCustomBack, 101
    );

    // cancel custom back behaviour
    $scope.$on('$destroy', function() {
        deregister();
        MapsService.clearWatch();
    });

    var deregister = function () {
      deregisterHardBack();
      deregisterSoftBack();
    }

    $scope.$on('$ionicView.enter', function() {
      if(JSON.stringify($localStorage.fiscalizacionGlobal) == "{}"){
        $scope.esNuevo = true;
      }else{
        $scope.esNuevo = false;
      }

      // code to run each time view is entered
        $scope.modoConsulta = function(){
        	var ultimo = undefined
        	if($scope.obra.fiscalizaciones){
        	 	ultimo = $scope.obra.fiscalizaciones[$scope.obra.fiscalizaciones.length -1];
        	}
            if($scope.esNuevo){
                    return false;
            }if(ultimo && ultimo.fecha_intervencion == $localStorage.fiscalizacionGlobal.fecha_intervencion){
                    return false;
            }else{
                    return true;
            }
        };
        $scope.actualizando = function(){
            if($localStorage.fiscalizacionGlobal.estado){
                if($localStorage.fiscalizacionGlobal.estado == 'actualizando'){
                    return true;
                }else{
                    return false;
                }
            }else{
                if($scope.esNuevo){
                    return false;
                }else{
                    return true;
                }
            }
        };

        $scope.recuperando = $localStorage.recuperando;

        $scope.obra = $localStorage.obraGlobal;
        $scope.local = $localStorage.localGlobal;

        if($localStorage.recuperando){
          $scope.fiscalizacion = angular.copy($localStorage.fiscalizacionBackup);
          $ionicSideMenuDelegate.canDragContent(false);
        }else{
          $scope.fiscalizacion = angular.copy($localStorage.fiscalizacionGlobal);
        }
        if(!$scope.modoConsulta()){
          $localStorage.fiscalizacionBackup = $scope.fiscalizacion;
        }

        $scope.items = $scope.obra.items;
        //$scope.fiscalizacion.origen_app = true;

        //Se cargan los avances como informacion de cada item
       console.log('es nuevo:' + $scope.esNuevo);
       if(!$localStorage.recuperando){


        if(!$scope.esNuevo){
            for(i in $scope.fiscalizacion.avances){
                var avance = $scope.fiscalizacion.avances[i];
                avance.id_avance = $scope.fiscalizacion.id_avance;
              for(j in $scope.items){
                    var item = $scope.items[j];
                    if(item.id_item == avance.id_item){
                        avance.item = item.item.descripcion;
                        avance.unidad_medida = item.unidad_medida;
                        avance.cantidad = item.cantidad;
                    }
                }
            }
            if($scope.fiscalizacion.latitud){
                $scope.toggleMap = false;
                MapsService.createMapFromLatLong($scope);
            }
            if($scope.fiscalizacion.id_avance){
              //Verificar si en el listado de anexos ya se encuentran las firmas
              //Descargar los archivos y guardar en el dispositivo
              var encontroFirmaFiscalizador = false;
              var encontroFirmaContratista = false;
              for(i in $scope.fiscalizacion.anexos){
                if($scope.fiscalizacion.anexos[i].anexoFileName == 'fiscalizador'){
                  //Descargar foto firma fiscalizador
                  console.log('Encontrada firma fiscalizador');
                  encontroFirmaFiscalizador = true;
                  $scope.fiscalizacion.firmaFiscalizador = $scope.fiscalizacion.anexos[i];
                  FotosService.descargarFirma($scope, $scope.fiscalizacion.firmaFiscalizador, $scope.fiscalizacion, 'firmaFiscalizador.png');

                }
                if($scope.fiscalizacion.anexos[i].anexoFileName == 'contratista'){
                  encontroFirmaContratista = true;
                  //Descargar foto firma contratista
                  console.log('Encontrada firma contratista');
                  $scope.fiscalizacion.firmaContratista = $scope.fiscalizacion.anexos[i];
                  FotosService.descargarFirma($scope, $scope.fiscalizacion.firmaContratista, $scope.fiscalizacion, 'firmaContratista.png');
                }
              }

              if(!encontroFirmaFiscalizador && $scope.fiscalizacion.firmaFiscalizador){
                FotosService.descargarFirma($scope, $scope.fiscalizacion.firmaFiscalizador, $scope.fiscalizacion, 'firmaFiscalizador.png');
              }
              if(!encontroFirmaContratista && $scope.fiscalizacion.firmaContratista){
                FotosService.descargarFirma($scope, $scope.fiscalizacion.firmaContratista, $scope.fiscalizacion, 'firmaContratista.png');
              }

              var imagenes = [];

              for(i in $scope.fiscalizacion.anexos){
                //Las firmas se manejan a parte
                if($scope.fiscalizacion.anexos[i].anexoFileName != 'fiscalizador'
                  && $scope.fiscalizacion.anexos[i].anexoFileName != 'contratista'){
                  imagenes.push($scope.fiscalizacion.anexos[i]);
                }
              }
              $scope.allImages = imagenes;
              $scope.fiscalizacion.anexos = $scope.allImages;



            }

            if($scope.fiscalizacion.verificaciones){
              angular.forEach($scope.tipos_verificaciones , function(tipo) {
                for(i in tipo.opciones){
                  angular.forEach($scope.fiscalizacion.verificaciones , function(verificacion) {
                    if(tipo.opciones[i].id == verificacion.verificacion.id){
                      if(tipo.tipo_formulario.id == 1){//radio button
                        tipo.seleccionado = verificacion.verificacion.id;
                        tipo.valorDescriptivo = verificacion.valorDescriptivo;
                      }else if (tipo.tipo_formulario.id == 2){//checklist
                        tipo.opciones[i].seleccionado = verificacion.cumplimiento;
                      }else{//numero
                        tipo.opciones[i].valorNumerico = verificacion.valorNumerico;
                      }

                    }
                  });
                }
              });


            }
        }else{
        	var ultimaFiscalizacion = undefined;
        	if($scope.obra.fiscalizaciones){
        		ultimaFiscalizacion= $scope.obra.fiscalizaciones[$scope.obra.fiscalizaciones.length -1];
        	}
            var avance = {};
            $scope.fiscalizacion.avances = [];
            $scope.fiscalizacion.anexos = [];
            $scope.fiscalizacion.id_obra = $scope.obra.id_obra;
            var fecha = new Date();
            fecha.setHours(0,0,0,0);
            $scope.fiscalizacion.fecha_intervencion = fecha.getTime();
            for(j in $scope.items){
                var item = $scope.items[j];
                avance = {};
                avance.id_avance = $scope.fiscalizacion.id_avance;
                avance.id_item = item.id_item;
                avance.item = item.item.descripcion;
                avance.unidad_medida = item.unidad_medida;
                avance.cantidad = item.cantidad;
                if(ultimaFiscalizacion){
                	for(i in ultimaFiscalizacion.avances){
                        var aux = ultimaFiscalizacion.avances[i];
                        if(item.id_item == aux.id_item){
                                avance.factor_intervencion = aux.factor_intervencion;
                        }
	                }
                }else{
                	avance.factor_intervencion = 0;
                }

                $scope.fiscalizacion.avances.push(avance);
            }
            //$scope.obtenerUbicacion();
        }
       }else{
         $scope.firma = localStorage.getItem('firma');
         $scope.firmaContratista = localStorage.getItem('firmaContratista');
       }
        $scope.borradas = [];
        $scope.fotosToggle = false;
    });

    function formattedDateString(d) {
        if(!d){
            d = new Date();
        }

        var month = String(d.getMonth() + 1);
        var day = String(d.getDate());
        var year = String(d.getFullYear());

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return day+'/'+month+'/'+year;
    }

    function formattedStringDate(str){
        if(!str){
            str = formattedDateString();
        }
        var dmy = str.split("/");
        var d = new Date(dmy[1]+'/'+dmy[0]+'/'+dmy[2] );
        return d;
    }

	var opcionesDatePicker = {
      callback: function (val) {  //Mandatory
      	$scope.fiscalizacion.fecha_intervencion = val;
      },
      from: new Date(2012, 1, 1), //Optional
      to: new Date(), //Optional
      //inputDate: $localStorage.fiscalizacionGlobal.fecha_intervencion,      //Optional
      //mondayFirst: true,          //Optional
      //disableWeekdays: [0],       //Optional
      closeOnSelect: false
    };

    $scope.openDatePicker = function(){
      opcionesDatePicker.inputDate= new Date($scope.fiscalizacion.fecha_intervencion);
      ionicDatePicker.openDatePicker(opcionesDatePicker);
    };

    /*
    $scope.onezoneDatepicker = {
        date: $localStorage.fiscalizacionGlobal.fecha_intervencion, // MANDATORY
        mondayFirst: false,
        months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        daysOfTheWeek: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sa'],
        //startDate: startDate,
        endDate: new Date(),
        disablePastDays: false,
        disableSwipe: false,
        disableWeekend: false,
        //disableDates: disableDates,
        //disableDaysOfWeek: disableDaysOfWeek,
        showDatepicker: false,
        showTodayButton: true,
        calendarMode: false,
        hideCancelButton: false,
        hideSetButton: false,
        //highlights: highlights,
        callback: function(value){
            $scope.fiscalizacion.fecha_intervencion = value;
        }
    };
    */

	$scope.showImages = function(index) {
		$scope.activeSlide = index;
		$scope.sliderOptions = {
			initialSlide: index,
			direction: 'horizontal', //or vertical
			speed: 300, //0.3s transition
			//autoplay: 2000,//change every second
			grabCursor: true //this replaces cursor by a hand when hover slider
		}
		$scope.showModal('templates/image-popover.html');
	}

	$scope.showModal = function(templateUrl) {
		$ionicModal.fromTemplateUrl(templateUrl, {
			scope: $scope,
			animation: 'slide-in-up'
		}).then(function(modal) {
			$scope.modal = modal;
			$scope.modal.show();
		});
	}
 	var borrando = false;
	// Close the modal
	$scope.closeModal = function() {
		if(!borrando){
			$scope.modal.hide();
			$scope.modal.remove();
		}
		borrando = false;
	};

	var borrarImagen = function(index) {
		var i = $scope.allImages.indexOf(index);
		if($scope.allImages.length == 1){
			$scope.modal.hide();
			$scope.modal.remove();
		}else{
			if($scope.allImages[i+1]){
				//$ionicSlideBoxDelegate.slide(i+1);
			}else{
				$ionicSlideBoxDelegate.previous();
			}
		}
		if (i > -1) {
			$scope.borradas.push(index.id_anexo);
		    $scope.allImages.splice(i, 1);
		}
		$ionicSlideBoxDelegate.update();
	};

	$scope.borrar = function(index) {
		borrando = true;
		if(window.cordova){
			$cordovaDialogs.confirm('¿Está seguro que quiere eliminar esta imágen?','Eliminar',
				['Aceptar','Cancelar'])
				.then(function(buttonIndex) {
				// no button = 0, 'OK' = 1, 'Cancel' = 2
				var btnIndex = buttonIndex;
				if(btnIndex == 1){
					borrarImagen(index);
				}
			});
	    }else{//Solo para pruebas
	    	var ok = confirm('¿Está seguro que quiere eliminar esta imágen?');
	    	if(ok){
				borrarImagen(index);
			}
	    }
	 };

	var numeroFoto = 0;
	$scope.takePicture = function() {
        var options = {
            quality : 80,
            //destinationType : Camera.DestinationType.DATA_URL,
            destinationType : Camera.DestinationType.FILE_URI,
            sourceType : Camera.PictureSourceType.CAMERA,
            allowEdit : false,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 300,
            targetHeight: 300,
            popoverOptions: CameraPopoverOptions,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };

        $cordovaCamera.getPicture(options).then(function(sourcePath) {
            //$scope.imgURI = "data:image/jpeg;base64," + imageData;
            var foto = {'temp': sourcePath};
            FotosService.copiarFotoTemp(foto, $scope);


        }, function(err) {
            // An error occured. Show a message to the user
        });
    }

    $ionicModal.fromTemplateUrl('templates/fiscalizacion_item_page.html', {
    scope: $scope,
    animation: 'slide-in-up'
	  }).then(function(modal) {
	    $scope.modalEdit = modal;
	    //$scope.openModal();
	  });

	$scope.formatearPorcentaje = function(porcentaje){
		if(porcentaje)
			return porcentaje.toFixed(2).replace(/0+$/, "");
	}

	 $scope.cargarDetalle = function(item) {
	 	$scope.situaciones = $localStorage.situaciones;

	 	$scope.itemEditar = item;
	  	$scope.data = {};
		$scope.data.factor_intervencion = item.factor_intervencion;
		if(item.id_situacion){
			$scope.data.id_situacion = item.id_situacion + '';
		}else{
			$scope.data.id_situacion = $scope.situaciones[0].id +'';
		}
		$scope.data.titulo = item.item;
		$scope.data.cantidad = item.cantidad;
		//console.log(typeof $scope.data.id_situacion);


	    $scope.modalEdit.show();
	  };

	  $scope.guardarDetalle = function(){
	  	if($scope.data.factor_intervencion > $scope.data.cantidad){
	  		//toastr.warning('El avance no puede ser mayor que la meta');
        $cordovaDialogs.alert('El avance no puede ser mayor que la meta','Advertencia');
        $scope.data.factor_intervencion = $scope.data.cantidad;

	  	}else{
	  		$scope.itemEditar.factor_intervencion = $scope.data.factor_intervencion;
		  	$scope.itemEditar.id_situacion = parseInt($scope.data.id_situacion);
		  	$scope.itemEditar.observacion = $scope.data.observacion;

		  	$scope.modalEdit.hide();
	  	}

	  }
	  $scope.cancelarDetalle = function() {
	    $scope.modalEdit.hide();
	  };
	  // Cleanup the modal when we're done with it!
	  $scope.$on('$destroy', function() {
	    $scope.modalEdit.remove();
	  });

	$scope.colorEstado = function(){
        var estado = '';
        if($scope.data == undefined){
        	return estado;
        }
        var factor = $scope.data.factor_intervencion;
        var cantidad = $scope.itemEditar.cantidad;
        $scope.data.porcentaje = (factor/cantidad)*100;
        if($scope.data.porcentaje >= 100){
        	estado = 'balanced';
        }else if(( $scope.data.porcentaje >= 50 && $scope.data.porcentaje < 100)){
        	estado = '';
        }else if ($scope.data.porcentaje > 0){
        	estado = '';
        }else {
        	estado = '';
        }
		return estado;
	}
	$scope.iconoEstado = function(){
        var estado = 'ion-ios-circle-outline';
        if($scope.data == undefined){
        	return estado;
        }
        var factor = $scope.data.factor_intervencion;
        var cantidad = $scope.itemEditar.cantidad;
        $scope.data.porcentaje = (factor/cantidad)*100;
        if($scope.data.porcentaje >= 100){
        	estado = 'ion-ios-checkmark';
        }else if(( $scope.data.porcentaje >= 50 && $scope.data.porcentaje < 100)){
        	estado = 'ion-ios-circle-filled';
        }else if ($scope.data.porcentaje > 0){
        	estado = 'ion-ios-circle-filled';
        }else {
        	estado = 'ion-ios-circle-outline';
        }
		return estado;
	}
	$scope.iconoEstadoDetalle = function(item){
        var estado = ' ion-ios-checkmark-outline';
        if(item == undefined){
        	return estado;
        }
        var factor = item.factor_intervencion;
        var cantidad = item.cantidad;
        var porcentaje = (factor/cantidad)*100;
        if(porcentaje >= 100){
        	estado = 'balanced ion-ios-checkmark';
        }else if(( porcentaje >= 50 && porcentaje < 100)){
        	estado = ' ion-ios-circle-filled';
        }else if (porcentaje > 0){
        	estado = ' ion-ios-circle-filled';
        }else {
        	estado = 'ion-ios-circle-outline';
        }
		return estado;
	}

    //Integracion de google maps
	$scope.textoObtenerUbicacion = "Obtener";
	$scope.botonObtenerUbicacion = "ion-ios-location";

	$scope.obtenerUbicacion = function(){
		MapsService.createMap($scope);
	};

	//createMap();

	$scope.toggler = function(toggle){
		if(toggle == true){
			if($scope.mapaIniciado == undefined){
				if($scope.modoConsulta()){
					if($scope.fiscalizacion.latitud){
						MapsService.createMapFromLatLong($scope);
						$scope.mapaIniciado = true;
					}
				}else{
					if($scope.fiscalizacion.latitud == undefined){
						$scope.obtenerUbicacion();
					}else{
						MapsService.createMapFromLatLong($scope);
					}
					$scope.mapaIniciado = true;
				}

				$timeout(function(){
        			MapsService.resizeMap($scope)}, 200);
			}
		}
		return toggle;

	}
	var resizeMap = function($scope){
		if($scope.map){
			var center = $scope.map.getCenter();
			google.maps.event.trigger($scope.map, 'resize');
			$scope.map.setCenter(center);
		}
	}

	$scope.panelFoto;
	$scope.fotosToggler = function(toggle){
		toggle = !toggle;
		$scope.fotosToggle = toggle;
		if(toggle == true){
			$scope.panelFoto = true;
			if($scope.allImages){
			  console.log(angular.toJson($scope.allImages));
			  for(i in $scope.allImages){
          FotosService.descargarFoto($scope, $scope.allImages[i], $scope.fiscalizacion, $scope.allImages[i].id_anexo+'.jpg');
				}//);
			}

		}
		return toggle;
	}
	$scope.recargarImagenes = function(){
		if($scope.allImages){
			$scope.allImages.forEach(function(image){
				FotosService.descargarFoto($scope, image, $scope.fiscalizacion, image.id_anexo+'.jpg', true);
			});
		}
	}
	$scope.obtenerImagen = function(image){
		if($scope.fotosToggle){
			var src = '';
			if(image.src){
				src = image.src;
			}else{
				src = 'img/cargando.gif'
			}
		}
		return src;
	}

	$scope.guardarFiscalizacion = function(){
		//Validación de datos de localización
		if($scope.lastPosition != null && $scope.lastPosition.coords != undefined){
			$scope.fiscalizacion.latitud = $scope.lastPosition.coords.latitude;
			$scope.fiscalizacion.longitud = $scope.lastPosition.coords.longitude;
		}


		//Validaciones de fecha
		var errorFecha = false;
		if($scope.esNuevo){
      for(i in $scope.obra.fiscalizaciones){
        var fechaExistente = new Date($scope.obra.fiscalizaciones[i].fecha_intervencion);
        var fechaNueva = new Date($scope.fiscalizacion.fecha_intervencion);
        if(fechaExistente >= fechaNueva){
          errorFecha = true;
          continue;
        }
      }
    }else{
      for(i in $scope.obra.fiscalizaciones){
        if($scope.obra.fiscalizaciones[i].id_avance != $localStorage.fiscalizacionGlobal.id_avance){
          //console.log(angular.toJson($scope.obra.fiscalizaciones[i], true) + "-" +  angular.toJson($localStorage.fiscalizacionGlobal, true));
          var fechaExistente = new Date($scope.obra.fiscalizaciones[i].fecha_intervencion);
          var fechaNueva = new Date($scope.fiscalizacion.fecha_intervencion);
          if(fechaExistente >= fechaNueva){
            errorFecha = true;
            continue;
          }
        }
      }
    }

		if(($scope.fiscalizacion.latitud == null || $scope.fiscalizacion.longitud == null) && window.cordova) {
			//toastr.warning('Debe obtener los datos de ubicación en la sección "Anexos"');
      $cordovaDialogs.alert('Debe obtener los datos de ubicación en la sección "Anexos"','Advertencia');

    }else if(errorFecha){
			//toastr.warning('Fecha ya existe o es menor a existente');
      $cordovaDialogs.alert('Fecha ya existe o es menor a existente','Advertencia');
    }else if(($scope.allImages == undefined || $scope.allImages.length < 1) && window.cordova) {
			//toastr.warning('Debe adjuntar al menos una evidencia en la sección "Anexos"');
      $cordovaDialogs.alert('Debe adjuntar al menos una evidencia en la sección "Anexos"','Advertencia');
    }else{
			var puedeGuardar = false;
			//Se asigna un id falso para poder identificarle
			if(!$scope.fiscalizacion.id_avance){
				var id_avance = $localStorage.id_avance - 1;
				$scope.fiscalizacion.id_avance = id_avance;
			}
			//Se guardan las fotos a la memoria del telefono
		    if(window.cordova){
				FotosService.crearSubDirectorioFotos($scope.fiscalizacion.id_avance).then(function (success) {
					puedeGuardar = true;
				}).catch(function(response) {
					if(response.code == 12){//Solo si el directorio ya existe
						puedeGuardar = true;
					}
				}).finally(function() {
					if(puedeGuardar){
						FotosService.guardarFotos($scope.allImages, $scope.fiscalizacion.id_avance).then(function(success){
							console.log('Todas las fotos guardadas');
              if($localStorage.id_avance > $scope.fiscalizacion.id_avance){
                $localStorage.id_avance = $scope.fiscalizacion.id_avance;
              }

              $scope.fiscalizacion.anexos = $scope.allImages;
              $scope.allImages = [];

							//Prepara listado a borrar del servidor y asigna a la fiscalizacion
							//Borra las fotos que aún no se enviaron al servidor
							angular.forEach($scope.borradas , function(id_anexo) {
							  console.log(id_anexo);
								if(id_anexo > 0){
									if($scope.fiscalizacion.borradas == undefined){
                    $scope.fiscalizacion.borradas = [];
									}
                  $scope.fiscalizacion.borradas.push(id_anexo);
								}
								if(id_anexo){//Si es que ya se guardó alguna vez
									FotosService.borrarFoto($scope.fiscalizacion.id_avance, id_anexo).then(function (success) {
										console.log('Borrado');
								      }, function (error) {
								      	console.log('error');
								      });
								}
							});
              console.log(angular.toJson($scope.fiscalizacion.borradas, true));
							if($scope.obra.fiscalizaciones == undefined){
								$scope.obra.fiscalizaciones = [];
							}

							//Se preparan las verificaciones
              var verificaciones = [];
              console.log(angular.toJson($scope.tipos_verificaciones, true));
              angular.forEach($scope.tipos_verificaciones, function(tipo) {
                if(tipo.tipo_formulario.id == 1){//radio button
                  if(tipo.seleccionado){//Se crea el objeto solo si se selecciono algo
                    var verificacion = {};
                    verificacion.cumplimiento = true;
                    verificacion.verificacion = {};
                    verificacion.valorDescriptivo = tipo.valorDescriptivo;
                    angular.forEach(tipo.opciones, function(opcion) {
                      if(opcion.id == tipo.seleccionado){
                        verificacion.verificacion = opcion;
                      }
                    });

                    verificaciones.push(verificacion);
                  }
                }else if (tipo.tipo_formulario.id == 2){//checklist
                  angular.forEach(tipo.opciones, function(opcion) {
                    var verificacion = {};
                    if(opcion.seleccionado){
                      verificacion.cumplimiento = opcion.seleccionado;
                    }else{
                      verificacion.cumplimiento = false;
                    }

                    delete opcion.seleccionado;
                    verificacion.verificacion = opcion;

                    verificaciones.push(verificacion);
                  });
                }else{//numero
                  angular.forEach(tipo.opciones, function(opcion) {
                    if(opcion.valorNumerico){
                      var verificacion = {};
                      verificacion.valorNumerico = opcion.valorNumerico;
                      delete opcion.valorNumerico;
                      verificacion.verificacion = opcion;
                      verificacion.cumplimiento = true;
                      verificaciones.push(verificacion);
                    }
                  });
                }
              });

              console.log(verificaciones);
              $scope.fiscalizacion.verificaciones = verificaciones;
              $scope.fiscalizacion.updatedAt = (new Date()).getTime();
              if($scope.esNuevo){
								$scope.obra.fiscalizaciones.push($scope.fiscalizacion);
							}else{
								for(i in $scope.obra.fiscalizaciones){
									if($scope.obra.fiscalizaciones[i].id_avance == $scope.fiscalizacion.id_avance){
										$scope.obra.fiscalizaciones[i] = $scope.fiscalizacion;
										encontro = true;
										continue;
									}
								}
							}
							//Se le asigna un estado:
							//actualizando, si es una fiscalizacion que ya existe en el servidor
							//guardado, si es una nueva fiscalizacion
							if($scope.fiscalizacion.estado == undefined){
								if(!$scope.esNuevo){
						    		$scope.fiscalizacion.estado = 'actualizando';
						    	}else{
							    	$scope.fiscalizacion.estado = 'guardado';
							    }
							}

              //Guardar firmas
							FotosService.savefile(localStorage.getItem('firma'), $scope.fiscalizacion.id_avance, 'firmaFiscalizador');
							FotosService.savefile(localStorage.getItem('firmaContratista'), $scope.fiscalizacion.id_avance, 'firmaContratista');


              FotosService.vaciarDirectorioTemp();
              localStorage.setItem('firma', "");
              localStorage.setItem('firmaContratista', "");
              $localStorage.fiscalizacionBackup = {};
              if($localStorage.recuperando){
                for(i in $localStorage.locales){
                  for(j in $localStorage.locales[i].obras){
                    if ($localStorage.locales[i].obras[j].id_obra == $scope.obra.id_obra) {
                      $localStorage.locales[i].obras[j] = $scope.obra;
                    }
                  }
                }
                $ionicHistory.nextViewOptions({
                  disableBack: true
                });
                deregister();
                MapsService.clearWatch();
                $state.go("menu.locales");
              }else{
                $state.go('menu.fiscalizaciones');
              }
              $localStorage.recuperando = false;
							//toastr.info('Guardado para ser enviado', 'Guardado');
              $cordovaDialogs.alert('Guardado para ser enviado','Guardado');

						}, function(err){
							console.log(angular.toJson(err, true));
							if(err.code == 10){//No hay espacio en disco
                $cordovaDialogs.alert('Verificar espacio del dispositivo','Error');
                //toastr.error('verificar espacio del dispositivo', 'verificar espacio del dispositivo');
							}else{
                $cordovaDialogs.alert('Error al guardar fotos','Error');
                //toastr.error('Error al guardar fotos');
							}

						});

					}
				});
			}else{//solo para pruebas
          if($localStorage.id_avance > $scope.fiscalizacion.id_avance){
            $localStorage.id_avance = $scope.fiscalizacion.id_avance;
          }
				 $localStorage.fiscalizacionBackup = {};
          if($scope.obra.fiscalizaciones == undefined){
					$scope.obra.fiscalizaciones = [];
				}

          //Se preparan las verificaciones
          var verificaciones = [];

          angular.forEach($scope.tipos_verificaciones, function(tipo) {
            if(tipo.tipo_formulario.id == 1){//radio button
              if(tipo.seleccionado){//Se crea el objeto solo si se selecciono algo
                var verificacion = {};
                verificacion.cumplimiento = true;
                verificacion.verificacion = {};
                verificacion.valorDescriptivo = tipo.valorDescriptivo;
                angular.forEach(tipo.opciones, function(opcion) {
                  if(opcion.id == tipo.seleccionado){
                    verificacion.verificacion = opcion;
                  }
                });

                verificaciones.push(verificacion);
              }
            }else if (tipo.tipo_formulario.id == 2){//checklist
              angular.forEach(tipo.opciones, function(opcion) {
                var verificacion = {};
                if(opcion.seleccionado){
                  verificacion.cumplimiento = opcion.seleccionado;
                }else{
                  verificacion.cumplimiento = false;
                }
                verificacion.verificacion = opcion;

                verificaciones.push(verificacion);
              });
            }else{//numero
              angular.forEach(tipo.opciones, function(opcion) {
                if(opcion.valorNumerico){
                  var verificacion = {};
                  verificacion.valorNumerico = opcion.valorNumerico;
                  verificacion.verificacion = opcion;
                  verificacion.cumplimiento = true;
                  verificaciones.push(verificacion);
                }
              });
            }
          });

          console.log();
          $scope.fiscalizacion.verificaciones = verificaciones;

				if($scope.esNuevo){
					$scope.obra.fiscalizaciones.push($scope.fiscalizacion);
				}else{
					for(i in $scope.obra.fiscalizaciones){
            if($scope.obra.fiscalizaciones[i].id_avance == $scope.fiscalizacion.id_avance){
              $scope.obra.fiscalizaciones[i] = $scope.fiscalizacion;
							encontro = true;
							continue;
						}
					}
				}

				if($scope.fiscalizacion.estado == undefined){
					if(!$scope.esNuevo){
			    		$scope.fiscalizacion.estado = 'actualizando';
			    	}else{
				    	$scope.fiscalizacion.estado = 'guardado';
				    }
				}


        FotosService.vaciarDirectorioTemp();
        localStorage.setItem('firma', "");
        localStorage.setItem('firmaContratista', "");
        //cuando se esta recuperando no se actualizan las referencias del localStorage
        //se debe traer desde la raiz
        if($localStorage.recuperando){
          for(i in $localStorage.locales){
            for(j in $localStorage.locales[i].obras){
              if ($localStorage.locales[i].obras[j].id_obra == $scope.obra.id_obra) {
                $localStorage.locales[i].obras[j] = $scope.obra;
              }
            }
          }
          $ionicHistory.nextViewOptions({
            disableBack: true
          });
          deregister();
          MapsService.clearWatch();
          $state.go("menu.locales");
        }else{
          $state.go('menu.fiscalizaciones');
        }
        $localStorage.recuperando = false;
        $cordovaDialogs.alert('Agregado a pendientes','Guardado');

			}
		}
	}

	$scope.enviarFirma = function(){
		//var blob = b64toBlob(localStorage.getItem('firma'), 'image/png');
		//FotosService.uploadFirma(localStorage.getItem('firma'));
		FotosService.savefile(localStorage.getItem('firma'));
	}

	$ionicModal.fromTemplateUrl('templates/firma.html', {
    scope: $scope,
    animation: 'slide-in-up'
	  }).then(function(modal) {
	    $scope.firmaModal = modal;
	    //$scope.openModal();
	  });

	FirmaService.scope = $scope;
 	$scope.mostrarFirmaModal = function(firma){
    screen.orientation.lock('landscape');
    FirmaService.mostrarFirmaModal(firma);
 	};
 	$scope.cerrarFirmaModal = function() {
    FirmaService.cerrarFirmaModal();
	};
 	$scope.clearCanvas = function() {
		FirmaService.clearCanvas();
    };
    $scope.saveCanvas = function() {
        FirmaService.saveCanvas();
    };

  // cancel custom back behaviour
  $scope.$on('$destroy', function() {
    screen.orientation.lock('portrait');
    $scope.firmaModal.remove();
  });
  $scope.$on('modal.hidden', function(arg) {
    screen.orientation.lock('portrait');
    console.log(arg);
  });


})

.controller('certificadoCtrl', ['$scope', '$stateParams', '$cordovaFileOpener2', '$ionicLoading', '$cordovaFile', '$cordovaInAppBrowser',// The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams, $cordovaFileOpener2, $ionicLoading, $cordovaInAppBrowser) {
	var fileName = "prueba.pdf";
	var filePath = "https://drive.google.com/open?id=0B4HA2ur0H4XbVVhCeWw2LV9SWVU";
	var filePath2 = "";

	if(ionic.Platform.isAndroid()){
      	filePath2 = cordova.file.externalApplicationStorageDirectory + fileName;
      	//filePath2 = cordova.file.dataDirectory + fileName;
    }else if(ionic.Platform.isIOS()){
      	//filePath = cordova.file.dataDirectory + fileName;
      	filePath2 = cordova.file.dataDirectory + fileName;
    }

	$scope.download = function() {
	    $ionicLoading.show({
	      template: 'Loading...'
	    });

	    fileTransfer = new FileTransfer();
        fileTransfer.download(
			encodeURI("http://www.google.com/gestion_intervenciones/informes/emitir_informe_servidor?formato=pdf&nombre_reporte=rptActaMedicion.rptdesign&parametros=%26parametros0=2"),
            //encodeURI("http://ionicframework.com/img/ionic-logo-blog.png"),
            filePath2,
            function(entry) {
                $ionicLoading.hide();
                $scope.imgFile = filePath2;
              	alert("Download: " + entry.toURL());
            },
            function(error) {
                $ionicLoading.hide();
                alert("Download Error -> \nFilePath2: " + filePath2+ "\nError:" + JSON.stringify(error));
            },
            false,
            null
        );

	}

	$scope.openPDF = function() {
		 var options = {
	      location: 'no',
	      clearcache: 'yes',
	      toolbar: 'yes'
	    };
		$cordovaInAppBrowser.open(filePath, '_blank', options)
	      .then(function(event) {
	        // success
	      })
	      .catch(function(event) {
	      	alert("Error -> " + JSON.stringify(event));
	        // error
	      });
	};

	$scope.openPDF2 = function() {
	    $cordovaFileOpener2.open(
	        filePath2, // Any system location, you CAN'T use your appliaction assets folder
	        'application/pdf'
	    ).then(function() {
	        //alert('Success');
	    }, function(err) {
	        alert('An error occurred: ' + JSON.stringify(err));
	    });
	};

	$scope.tipo = $stateParams.tipo;
	$scope.titulo = "";
	$scope.src ="";

}])

.controller('guardadoCtrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
// You can include any angular dependencies as parameters for this function
// TIP: Access Route Parameters for your page via $stateParams.parameterName
function ($scope, $stateParams) {

})
