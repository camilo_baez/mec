angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'loginCtrl'
  })

  .state('menu.locales', {
    url: '/locales',
    views: {
      'menu': {
        templateUrl: 'templates/locales.html',
        controller: 'localesCtrl'
      }
    }
  })

  .state('menu.obras', {
    url: '/obras',
    params: {ob: null},
    views: {
      'menu': {
        templateUrl: 'templates/obras.html',
        controller: 'obrasCtrl'
      }
    }
  })

  .state('menu.fiscalizaciones', {
    url: '/fiscalizaciones',
    params: {ob: null},
    views: {
      'menu': {
        templateUrl: 'templates/fiscalizaciones.html',
        controller: 'fiscalizacionesCtrl'
      }
    }
  })

  .state('menu.fiscalizacion', {
    url: '/fiscalizacion',
    params: {ob: null},
    views: {
      'menu': {
        templateUrl: 'templates/fiscalizacion.html',
        controller: 'fiscalizacionCtrl'
      }
    }
  })

    .state('menu', {
    url: '/menu',
    templateUrl: 'templates/menu.html',
    controller: 'menuCtrl'
  })

  .state('menu.firma', {
    url: '/firma',
    views: {
      'menu': {
        templateUrl: 'templates/firma.html',
        controller: 'firmaCtrl'
      }
    }
  })

  .state('menu.firmaContratista', {
    url: '/firmaContratista',
    views: {
      'menu': {
        templateUrl: 'templates/firma_contratista.html',
        controller: 'firmaContratistaCtrl'
      }
    }
  })

  .state('menu.certificado', {
    url: '/certificado/:tipo',
    views: {
      'menu': {
        templateUrl: 'templates/pdf_viewer.html',
        controller: 'certificadoCtrl'
      }
    }
  })

  .state('guardado', {
    url: '/page7',
    templateUrl: 'templates/guardado.html',
    controller: 'guardadoCtrl'
  })

$urlRouterProvider.otherwise('/login')

});