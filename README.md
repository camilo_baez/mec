# Fiscalizaciones

## Versiones
nvm use 10.13.0

## Fix para build

> Agregar archivo platforms/android/build.extras.gradle con el siguiente contenido

```
configurations.all {
       resolutionStrategy {
           force 'com.android.support:support-v4:27.1.0'
       }
   }
```

Si no soluciona el anterior probar lo siguiente

Editar "platforms/android/project.properties"

cambiar 

> cordova.system.library.2=com.android.support:support-v4:+

por 

> cordova.system.library.2=com.android.support:support-v4:27.1.0

luego ejecutar

> cordova clean